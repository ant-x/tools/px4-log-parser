function str_log_dir_full = getLogDir( varargin )
%GETLOGDIR Retrieve default log directory.
%   str_log_dir = getLogDir() returns the default location str_log_dir
%   where the parsed logs are placed. 
%   This is the mat_logs directory within the root directory of
%   PX4_log_parser.
% 
%   str_log_dir = getLogDir('LogType', Value) returns the path to the
%   desired log directory, where Value may have the following values:
%       'raw':      directory containing raw (.ulg) log files
%       'parsed':   directory containing parsed (.mat) log files (default)

% Authors
% Simone Panza <simone@antx.it>
% Mattia Giurato <mattia@antx.it>
% ANT-X s.r.l.
% Date 2024/01/03

%%% INPUT PARSING

pars = inputParser;

checkLogType = @(x) ischar(x);
defaultLogType = 'parsed';

addOptional(pars, 'LogType', defaultLogType, checkLogType);

parse(pars, varargin{:});

log_type = pars.Results.LogType;

%%% END INPUT PARSING

STR_DIR_LOG_RAW = 'ulg_logs';
STR_DIR_LOG_PARSED = 'mat_logs';

if strcmp(log_type, 'raw')
    str_log_dir = STR_DIR_LOG_RAW;
elseif strcmp(log_type, 'parsed')
    str_log_dir = STR_DIR_LOG_PARSED;
else
    error('Invalid value for argument LogType: must be ''raw'' or ''parsed''.')
end

% recover the directory in which this file is located (i.e., the root
% directory of the repo)
str_file_path = mfilename('fullpath');
[str_file_dir, ~, ~] = fileparts(str_file_path);
str_root_dir = str_file_dir;

str_log_dir_full = fullfile(str_root_dir, str_log_dir);

end

%% END OF CODE
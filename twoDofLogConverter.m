function outputFileName = twoDofLogConverter( ulgFileName, varargin )
%TWODOFLOGCONVERTER Extract data from ULOG file for the ANT-X 2DoF Drone.
%
% This routine takes an input the .ulg file containing logged topics of a
% 2Dof Drone.
%
% INPUT
%   ulgFileName     a string containing the name of the .ulg file to be
%                   parsed (without extension)
%
% Additional input parameters
%   'InputLogFileDir'       directory containing the input .mat file
%   'OutputLogFileDir'      directory containing the output .mat file
%   'OutputFileName'        name of output .mat file
%   'defaultDownsample'     downsample the log data to 100 Hz
%
% OUTPUT
%   outputFileName  string of the name of .mat file containing results of
%                   parsing

%% INPUT PARSING
pars = inputParser;

containsNoSpaces = @(x) isempty(regexp(x, '\s', 'once')); % true if string x does not contain any whitespaces

checkFileName = @(x) ischar(x) & containsNoSpaces(x);

curDir = getCurrentDirectory();
defaultInputLogFileDir = fullfile(curDir, 'ulg_logs');
defaultOutputLogFileDir = fullfile(curDir, 'mat_logs');
defaultOutputFileName = strcat(ulgFileName,'_2dd');
defaultDownsample = 'false';

addRequired(pars, 'ulgFileName', checkFileName);
addParameter(pars, 'InputLogFileDir', defaultInputLogFileDir, checkFileName);
addParameter(pars, 'OutputLogFileDir', defaultOutputLogFileDir, checkFileName);
addParameter(pars, 'OutputFileName', defaultOutputFileName, checkFileName);
addParameter(pars, 'Downsample', defaultDownsample, checkFileName);

parse(pars, ulgFileName, varargin{:});

input_dir = pars.Results.InputLogFileDir;
output_dir = pars.Results.OutputLogFileDir;
out_file = pars.Results.OutputFileName;
downsample = pars.Results.Downsample;

if strcmp(downsample, 'true')
    fprintf('\nData will be downsampled to 100 Hz\n');
end

%% parse ulg file
outputFileName = ulg2mat(ulgFileName, 'RawLogFileDir', input_dir, 'MatLogFileDir', output_dir, 'OutputFileName', out_file);

%% load logged data
input_log = load(outputFileName);

%% parse data
% sensors
try
    parsedData.acc.timestamp = input_log.sensor_combined_0.timestamp;
    parsedData.acc.value = input_log.sensor_combined_0.accelerometer_m_s2;
catch
end

try
    parsedData.gyr.timestamp = input_log.sensor_combined_0.timestamp;
    parsedData.gyr.value = input_log.sensor_combined_0.gyro_rad;
catch
end

try
    parsedData.dist.timestamp = input_log.distance_sensor_0.timestamp;
    parsedData.dist.value = input_log.distance_sensor_0.current_distance;
catch
end

% states
try
    parsedData.q.timestamp = input_log.vehicle_angular_velocity_0.timestamp;
    parsedData.q.value = input_log.vehicle_angular_velocity_0.xyz(:,2);
catch
end

try
    parsedData.theta.timestamp = input_log.vehicle_attitude_0.timestamp;
    eul_attitude = quat2eul(input_log.vehicle_attitude_0.q,'ZYX');
    parsedData.theta.value = eul_attitude(:,2);
    clear eul_attitude;
catch
end

try
    parsedData.v.timestamp = input_log.vehicle_local_position_0.timestamp;
    parsedData.v.value = input_log.vehicle_local_position_0.vx;
catch
end

try
    parsedData.x.timestamp = input_log.vehicle_local_position_0.timestamp;
    parsedData.x.value = input_log.vehicle_local_position_0.x;
catch
end

% feedforward
try
    parsedData.thrust0.timestamp = input_log.flight_controller_setpoint_0.timestamp;
    parsedData.thrust0.value = input_log.flight_controller_setpoint_0.f_c(:,3);
catch
end

try
    MASK_EN_M = uint8(2);
    MASK_EN_T = uint8(32);
    
    parsedData.lmnf.timestamp = input_log.lmnf_injection_0.timestamp;
    parsedData.lmnf.M = input_log.lmnf_injection_0.m;
    parsedData.lmnf.T = input_log.lmnf_injection_0.f(:,3);
    parsedData.lmnf.openloop = boolean(input_log.lmnf_injection_0.type_openloop);
    parsedData.lmnf.enableM = bitand(uint8(input_log.lmnf_injection_0.mask),MASK_EN_M)>0;
    parsedData.lmnf.enableT = bitand(uint8(input_log.lmnf_injection_0.mask),MASK_EN_T)>0;
catch
end

% setpoints
try
    parsedData.q0.timestamp = input_log.flight_controller_internal_0.timestamp;
    parsedData.q0.value = input_log.flight_controller_internal_0.omega_r(:,2);
catch
end

try
    parsedData.theta0.timestamp = input_log.flight_controller_internal_0.timestamp;
    eul_attitude_sp = quat2eul(input_log.flight_controller_internal_0.q_r,'ZYX');
    parsedData.theta0.value = eul_attitude_sp(:,2);
    clear eul_attitude_sp;
catch
end

try
    parsedData.v0.timestamp = input_log.flight_controller_internal_0.timestamp;
    parsedData.v0.value = input_log.flight_controller_internal_0.v_r(:,1);
catch
end

try
    parsedData.x0.timestamp = input_log.flight_controller_internal_0.timestamp;
    parsedData.x0.value = input_log.flight_controller_internal_0.p_r(:,1);
catch
end

% control actions
try
    parsedData.M.timestamp = input_log.actuator_controls_2_0.timestamp;
    parsedData.M.value = input_log.actuator_controls_2_0.control(:,2);
catch
end

try
    parsedData.T.timestamp = input_log.actuator_controls_2_0.timestamp;
    parsedData.T.value = input_log.actuator_controls_2_0.control(:,4);
catch
end

if strcmp(downsample, 'true')    
    %% interpolate data
    % find initial time instant
    sample_time = 1/100;
    
    % sensors
    try
        time_vector = (parsedData.acc.timestamp(1):sample_time:parsedData.acc.timestamp(end))';
        parsedData.acc.value = interp1(parsedData.acc.timestamp, parsedData.acc.value, time_vector, 'previous');
        parsedData.acc.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.gyr.timestamp(1):sample_time:parsedData.gyr.timestamp(end))';
        parsedData.gyr.value = interp1(parsedData.gyr.timestamp, parsedData.gyr.value, time_vector, 'previous');
        parsedData.gyr.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.dist.timestamp(1):sample_time:parsedData.dist.timestamp(end))';
        parsedData.dist.value = interp1(parsedData.dist.timestamp, parsedData.dist.value, time_vector, 'previous');
        parsedData.dist.timestamp = time_vector;
    catch
    end
    
    % states
    try
        time_vector = (parsedData.q.timestamp(1):sample_time:parsedData.q.timestamp(end))';
        parsedData.q.value = interp1(parsedData.q.timestamp, parsedData.q.value, time_vector, 'previous');
        parsedData.q.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.theta.timestamp(1):sample_time:parsedData.theta.timestamp(end))';
        parsedData.theta.value = interp1(parsedData.theta.timestamp, parsedData.theta.value, time_vector, 'previous');
        parsedData.theta.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.v.timestamp(1):sample_time:parsedData.v.timestamp(end))';
        parsedData.v.value = interp1(parsedData.v.timestamp, parsedData.v.value, time_vector, 'previous');
        parsedData.v.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.x.timestamp(1):sample_time:parsedData.x.timestamp(end))';
        parsedData.x.value = interp1(parsedData.x.timestamp, parsedData.x.value, time_vector, 'previous');
        parsedData.x.timestamp = time_vector;
    catch
    end
    
    % feedforward
    try
        time_vector = (parsedData.thrust0.timestamp(1):sample_time:parsedData.thrust0.timestamp(end))';
        parsedData.thrust0.value = interp1(parsedData.thrust0.timestamp, parsedData.thrust0.value, time_vector, 'previous');
        parsedData.thrust0.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.lmnf.timestamp(1):sample_time:parsedData.lmnf.timestamp(end))';
        parsedData.lmnf.value = interp1(parsedData.lmnf.timestamp, parsedData.lmnf.value, time_vector, 'previous');
        parsedData.lmnf.timestamp = time_vector;
    catch
    end
    
    % setpoints
    try
        time_vector = (parsedData.q0.timestamp(1):sample_time:parsedData.q0.timestamp(end))';
        parsedData.q0.value = interp1(parsedData.q0.timestamp, parsedData.q0.value, time_vector, 'previous');
        parsedData.q0.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.theta0.timestamp(1):sample_time:parsedData.theta0.timestamp(end))';
        parsedData.theta0.value = interp1(parsedData.theta0.timestamp, parsedData.theta0.value, time_vector, 'previous');
        parsedData.theta0.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.v0.timestamp(1):sample_time:parsedData.v0.timestamp(end))';
        parsedData.v0.value = interp1(parsedData.v0.timestamp, parsedData.v0.value, time_vector, 'previous');
        parsedData.v0.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.x0.timestamp(1):sample_time:parsedData.x0.timestamp(end))';
        parsedData.x0.value = interp1(parsedData.x0.timestamp, parsedData.x0.value, time_vector, 'previous');
        parsedData.x0.timestamp = time_vector;
    catch
    end
    
    % control actions
    try
        time_vector = (parsedData.M.timestamp(1):sample_time:parsedData.M.timestamp(end))';
        parsedData.M.value = interp1(parsedData.M.timestamp, parsedData.M.value, time_vector, 'previous');
        parsedData.M.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.T.timestamp(1):sample_time:parsedData.T.timestamp(end))';
        parsedData.T.value = interp1(parsedData.T.timestamp, parsedData.T.value, time_vector, 'previous');
        parsedData.T.timestamp = time_vector;
    catch
    end   
end

%% save to .mat file
if exist(output_dir, 'dir') == 0
    mkdir(output_dir)
end

outputFileName = fullfile(output_dir, sprintf('%s.mat', out_file));

if exist(outputFileName, 'file') == 2
    delete(outputFileName)
end

save(outputFileName, '-struct', 'parsedData');

end

function curDir = getCurrentDirectory()
% GETCURRENTDIRECTORY returns the path in the filesystem of this file

path_routine = mfilename('fullpath'); % get full path name of the current routine
PATH_DEPTH = 0; % the default dirs are located at the same level of this routine
% (e.g., PATH_DEPTH = 1 means a subdirectory wrt the current level)

ind_backslash_vec = regexp(path_routine, filesep);
ind_backslash = ind_backslash_vec(end - PATH_DEPTH);

curDir = path_routine(1:ind_backslash-1);

end

%% END OF CODE
function plotLoggedDataPx4( str_fileName, varargin )
%PLOTLOGGEDDATAPX4 Extracts data from log and plot.
%
% This routine takes an input the .mat file containing logged topics
% extracted from the ULOG file (see ulg2mat()).
%
% The routine reads the list of available topics from the .mat file and
% plots them.
% You can customize the plotting behaviour by adding more topics and/or
% modifying the existing plotting functions in this file.
%
% INPUT
%   str_fileName:   string containing absolute path to log file (.mat)
%   mask:           (optional) a string to control output behaviour
%                   (default: 'APC').
%
% By default, all logged quantities will be plotted. You can choose among
% the following masks (and any combination of them):
%   'A'     plot quantities related to attitude angles, rates and their
%           setpoints;
%   'P'     plot quantities related to position, velocity, their setpoints,
%           and the trajectory;
%   'C'     plot control actions;
%   'D'     plots figures for debugging purposes (e.g., plots measured vs
%           estimated position).
%
% Example: plot attitude and control actions
%   plotLoggedDataPx4( str_fileName, 'AC')
%
% Authors
% Simone Panza <simone@antx.it>
% Mattia Giurato <mattia@antx.it>
% ANT-X s.r.l.
% Date 2024/01/03
%
% Changelog:
%
%   v9
%       removed dependency from custom attitude library and update topics
%   v8
%       add 'D' option to mask with corresponding plotting routines
%   v7
%       add actuator PWM plot
%   v6
%       remove plot of body-fixed velocity (of little use)
%   v5
%       fix bug with MATLAB2018b implementation of legend(): it will cause
%       problems when one of the entries in the list is 'position'
%   v4
%       improve trajectory plots (start/ending points)
%   v3
%       added optional mask to control which variables to plot
%   v2
%       small fix to control mode plot:
%       first plot attitude, then altitude and then position
%       modified ylim
%   v1
%       first version

%%% INPUT PARSING

pars = inputParser;

checkFileName = @(x) ischar(x);

checkMask = @(x) ischar(x);
defaultMask = 'APC';

addRequired(pars, 'str_fileName', checkFileName);
addOptional(pars, 'mask', defaultMask, checkMask);

parse(pars, str_fileName, varargin{:});

mask = pars.Results.mask;

%%% END INPUT PARSING

%%% PARSE MASK
if ~isempty(regexp(mask, 'A', 'once'))
    mask_attitude = true;
else
    mask_attitude = false;
end

if ~isempty(regexp(mask, 'P', 'once'))
    mask_position = true;
else
    mask_position= false;
end

if ~isempty(regexp(mask, 'C', 'once'))
    mask_control = true;
else
    mask_control= false;
end

if ~isempty(regexp(mask, 'D', 'once'))
    mask_debug = true;
else
    mask_debug= false;
end

%%% CONSTANTS: TOPIC NAMES
%%% normal topics
STR_NAME_TOPIC_ATTITUDE = 'vehicle_attitude_0';
STR_NAME_TOPIC_ATTITUDE_SETPOINT = 'vehicle_attitude_setpoint_0';
STR_NAME_TOPIC_RATE_SETPOINT = 'vehicle_rates_setpoint_0';
STR_NAME_TOPIC_ANGULAR_RATES = 'vehicle_angular_velocity_0';
STR_NAME_TOPIC_POSITION = 'vehicle_local_position_0';
STR_NAME_TOPIC_POSITION_SETPOINT = 'vehicle_local_position_setpoint_0';
STR_NAME_TOPIC_CONTROL_MODE = 'vehicle_control_mode_0';
STR_NAME_TOPIC_CONTROL_ACTIONS = 'actuator_controls_0_0';
STR_NAME_TOPIC_MOCAP = 'att_pos_mocap_0';
STR_NAME_TOPIC_ODOM = 'vehicle_visual_odometry_0';
STR_NAME_TOPIC_GPS = 'vehicle_gps_position_0';
STR_NAME_TOPIC_GLOBAL_POSITION = 'vehicle_global_position_0';
STR_NAME_TOPIC_SENSORS = 'sensor_combined_0';
STR_NAME_TOPIC_CONTROL_PWM = 'actuator_outputs_0';

%%% custom topics
% STR_NAME_TOPIC_FC_SETPOINT = 'flight_controller_setpoint_0';
STR_NAME_TOPIC_FC_SETPOINT = 'flight_controller_internal_0';
STR_NAME_TOPIC_FC_CONTROL_ACTIONS = 'fc_control_actions_monitor_0';
STR_NAME_TOPIC_FC_MOTORS = 'actuator_controls_0_0';
STR_NAME_TOPIC_FC_SERVOS = 'actuator_controls_1_0';
STR_NAME_TOPIC_FC_STATE = 'flight_state_0';
STR_NAME_TOPIC_WING_ANGLE = 'wing_angle_0';
STR_NAME_TOPIC_MANUAL_CONTROL = 'manual_control_setpoint_0';

%%% CONSTANTS: CONTROLLER MODE
CONTROLLER_MODE_NORMAL = 'NORMAL';
CONTROLLER_MODE_CUSTOM = 'CUSTOM';

% patch: disable warnings
STR_ID_WARNING_SIM_NONZERO_INIT_TIME = 'Control:analysis:LsimStartTime';

%%% load logged data
res = load(str_fileName);

loggedTopicNames = fieldnames(res);

%%% determine controller mode
if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_FC_SETPOINT)
    controller_mode = CONTROLLER_MODE_CUSTOM;
else
    controller_mode = CONTROLLER_MODE_NORMAL;
end

%%% PLOT VARIABLES

% attitude and angular rates
if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_ATTITUDE) && mask_attitude
    h = figure;
    plot_attitude(res.(STR_NAME_TOPIC_ATTITUDE), h);

%     hr = figure;
%     plot_rates(res.(STR_NAME_TOPIC_ATTITUDE), hr);
end

if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_ANGULAR_RATES) && mask_attitude
    h = figure;
    plot_rates(res.(STR_NAME_TOPIC_ANGULAR_RATES), h);
end

% attitude vs setpoint
if strcmp(controller_mode, CONTROLLER_MODE_NORMAL)
    if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_ATTITUDE_SETPOINT) && isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_ATTITUDE) && mask_attitude
        h = figure;
        plot_attitude_vs_setpoint(res.(STR_NAME_TOPIC_ATTITUDE), res.(STR_NAME_TOPIC_ATTITUDE_SETPOINT), h);
    end
end

if strcmp(controller_mode, CONTROLLER_MODE_CUSTOM)
    if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_FC_SETPOINT) && isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_ATTITUDE) && mask_attitude
        h = figure;
        plot_attitude_vs_setpoint_custom(res.(STR_NAME_TOPIC_ATTITUDE), res.(STR_NAME_TOPIC_FC_SETPOINT), h);
    end
end

% rate vs setpoint
if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_RATE_SETPOINT) && isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_ANGULAR_RATES) && mask_attitude
    hr = figure;
    plot_rates_vs_setpoints(res.(STR_NAME_TOPIC_ANGULAR_RATES), res.(STR_NAME_TOPIC_RATE_SETPOINT), hr);
end

% position, velocity, trajectory
if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_POSITION) && mask_position
    h = figure;
    plot_position(res.(STR_NAME_TOPIC_POSITION), h);

    hv = figure;
    plot_velocity(res.(STR_NAME_TOPIC_POSITION), hv);

    ht = figure;
    plot_trajectory(res.(STR_NAME_TOPIC_POSITION), ht);

    ht3 = figure;
    plot_trajectory_3d(res.(STR_NAME_TOPIC_POSITION), ht3);
end

% position and velocity vs setpoints
if strcmp(controller_mode, CONTROLLER_MODE_NORMAL)
    if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_POSITION_SETPOINT) && isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_POSITION) && mask_position
        h = figure;
        plot_position_vs_setpoint(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_POSITION_SETPOINT), h);

        hv = figure;
        plot_velocity_vs_setpoint(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_POSITION_SETPOINT), hv);

        hvb = figure;
        plot_body_velocity_vs_setpoint(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_POSITION_SETPOINT), res.(STR_NAME_TOPIC_ATTITUDE), hvb);

        hcs = figure;
        plot_cruise_speed_vs_setpoint(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_POSITION_SETPOINT), hcs);

        ht = figure;
        plot_trajectory_vs_setpoint(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_POSITION_SETPOINT), ht);

        ht3 = figure;
        plot_trajectory_3d_vs_setpoint(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_POSITION_SETPOINT), ht3);
    end
end

if strcmp(controller_mode, CONTROLLER_MODE_CUSTOM)
    if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_FC_SETPOINT) && isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_POSITION) && mask_position
        h = figure;
        plot_position_vs_setpoint_custom(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_FC_SETPOINT), h);

        hv = figure;
        plot_velocity_vs_setpoint_custom(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_FC_SETPOINT), hv);

        hvb = figure;
        plot_body_velocity_vs_setpoint_custom(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_FC_SETPOINT), res.(STR_NAME_TOPIC_ATTITUDE), hvb);

        hcs = figure;
        plot_cruise_speed_vs_setpoint_custom(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_FC_SETPOINT), hcs);

        ht = figure;
        plot_trajectory_vs_setpoint_custom(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_FC_SETPOINT), ht);

        ht3 = figure;
        plot_trajectory_3d_vs_setpoint_custom(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_FC_SETPOINT), ht3);
    end
end

% control actions
if strcmp(controller_mode, CONTROLLER_MODE_NORMAL)
    if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_CONTROL_ACTIONS) && mask_control
        h = figure;
        plot_moments(res.(STR_NAME_TOPIC_CONTROL_ACTIONS), h);

        ht = figure;
        plot_thrust(res.(STR_NAME_TOPIC_CONTROL_ACTIONS), ht);
    end
end

if strcmp(controller_mode, CONTROLLER_MODE_CUSTOM)
    if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_FC_CONTROL_ACTIONS) && mask_control
        h = figure;
        plot_moments_custom(res.(STR_NAME_TOPIC_FC_CONTROL_ACTIONS), h);

        hf = figure;
        plot_forces_custom(res.(STR_NAME_TOPIC_FC_CONTROL_ACTIONS), hf);

        ht = figure;
        plot_thrust_custom(res.(STR_NAME_TOPIC_FC_CONTROL_ACTIONS), res.(STR_NAME_TOPIC_FC_STATE), ht);
    end
end

% motors and servos control actions
if strcmp(controller_mode, CONTROLLER_MODE_CUSTOM)
    if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_FC_MOTORS) && mask_control
        h = figure;
        plot_motors_custom(res.(STR_NAME_TOPIC_FC_MOTORS), h);
    end


    if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_FC_SERVOS) && mask_control
        h = figure;
        plot_servos_custom(res.(STR_NAME_TOPIC_FC_SERVOS), h);
    end
end

% motor PWM
if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_CONTROL_PWM) && mask_control
    h = figure;
    plot_pwm(res.(STR_NAME_TOPIC_CONTROL_PWM), h);
end

% control mode
if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_CONTROL_MODE)
    h = figure;
    plot_control_mode(res.(STR_NAME_TOPIC_CONTROL_MODE), h);
end

% debug: compare mocap measurements vs estimates
if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_MOCAP) && isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_ATTITUDE) && mask_debug && mask_attitude
    h = figure;
    plot_attitude_mocap(res.(STR_NAME_TOPIC_ATTITUDE), res.(STR_NAME_TOPIC_MOCAP), h);
end

if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_ODOM) && isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_ATTITUDE) && mask_debug && mask_attitude
    h = figure;
    plot_attitude_odom(res.(STR_NAME_TOPIC_ATTITUDE), res.(STR_NAME_TOPIC_ODOM), h);
end

if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_MOCAP) && isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_POSITION) && mask_debug && mask_position
    h = figure;
    plot_position_mocap(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_MOCAP), h);

    hv = figure;
    plot_velocity_mocap(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_MOCAP), hv, STR_ID_WARNING_SIM_NONZERO_INIT_TIME)
end

if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_ODOM) && isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_POSITION) && mask_debug && mask_position
    h = figure;
    plot_position_odom(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_ODOM), h);

    hv = figure;
    plot_velocity_odom(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_ODOM), hv, STR_ID_WARNING_SIM_NONZERO_INIT_TIME)
end


if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_GPS) && isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_POSITION) && mask_debug && mask_position
    h = figure;

    plot_velocity_gps(res.(STR_NAME_TOPIC_POSITION), res.(STR_NAME_TOPIC_GPS), h);
end

if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_SENSORS) && ...
        isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_GPS) && ...
        isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_GLOBAL_POSITION) && mask_debug && mask_position
    hp = figure;

    plot_altitude_gps(res.(STR_NAME_TOPIC_SENSORS), ...
        res.(STR_NAME_TOPIC_GPS), ...
        res.(STR_NAME_TOPIC_GLOBAL_POSITION), ...
        hp);

end

if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_GPS) && ...
        isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_POSITION) && ...
        mask_debug && mask_position

    hp1 = figure;
    plot_position_gps_local(res.(STR_NAME_TOPIC_GPS), ...
        hp1)
% 
%     hp2 = figure;
%     plot_position_gps_local_trajectory(res.(STR_NAME_TOPIC_GPS), ...
%         hp2)
% 
    hp3 = figure;
    plot_position_gps_vs_local(res.(STR_NAME_TOPIC_GPS), ...
        res.(STR_NAME_TOPIC_POSITION), ...
        hp3)

end

% wing angle 
if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_WING_ANGLE) && isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_MANUAL_CONTROL)
    h = figure;
    plot_wing_angle(res.(STR_NAME_TOPIC_WING_ANGLE), res.(STR_NAME_TOPIC_MANUAL_CONTROL), h);
end

end

%% PLOT FUNCTIONS
function plot_attitude(vehicle_attitude, h_fig)

eul_attitude = quat2eul(vehicle_attitude.q,'ZYX');

figure(h_fig)
hold on
plot(vehicle_attitude.timestamp, eul_attitude(:,3)/pi*180);
plot(vehicle_attitude.timestamp, eul_attitude(:,2)/pi*180);
plot(vehicle_attitude.timestamp, eul_attitude(:,1)/pi*180);

legend({'phi', 'theta', 'psi'})
grid
title('Attitude angles')
xlabel('time [s]')
ylabel('[deg]')

end

function plot_attitude_mocap(vehicle_attitude, mocap, h_fig)

eul_attitude = quat2eul(vehicle_attitude.q,'ZYX');
eul_attitude_mocap = quat2eul(mocap.q,'ZYX');

figure(h_fig)

subplot(311)
hold on
plot(mocap.timestamp, eul_attitude_mocap(:,3)/pi*180);
plot(vehicle_attitude.timestamp, eul_attitude(:,3)/pi*180);
grid
ylabel('phi [deg]')
legend('mocap', 'estimate')
title('Attitude angles: estimate vs mocap')

subplot(312)
hold on
plot(mocap.timestamp, eul_attitude_mocap(:,2)/pi*180);
plot(vehicle_attitude.timestamp, eul_attitude(:,2)/pi*180);
grid
ylabel('theta [deg]')
% legend('estimate', 'mocap')

subplot(313)
hold on
plot(mocap.timestamp, eul_attitude_mocap(:,1)/pi*180);
plot(vehicle_attitude.timestamp, eul_attitude(:,1)/pi*180);
grid
ylabel('psi [deg]')
% legend('estimate', 'mocap')

xlabel('time [s]')

end

function plot_attitude_odom(vehicle_attitude, odom, h_fig)

eul_attitude = quat2eul(vehicle_attitude.q,'ZYX');
eul_attitude_mocap = quat2eul(odom.q,'ZYX');

figure(h_fig)

subplot(311)
hold on
plot(odom.timestamp, eul_attitude_mocap(:,3)/pi*180);
plot(vehicle_attitude.timestamp, eul_attitude(:,3)/pi*180);
grid
ylabel('phi [deg]')
legend('odom', 'estimate')
title('Attitude angles: estimate vs mocap')

subplot(312)
hold on
plot(odom.timestamp, eul_attitude_mocap(:,2)/pi*180);
plot(vehicle_attitude.timestamp, eul_attitude(:,2)/pi*180);
grid
ylabel('theta [deg]')
% legend('estimate', 'mocap')

subplot(313)
hold on
plot(odom.timestamp, eul_attitude_mocap(:,1)/pi*180);
plot(vehicle_attitude.timestamp, eul_attitude(:,1)/pi*180);
grid
ylabel('psi [deg]')
% legend('estimate', 'mocap')

xlabel('time [s]')

end

function plot_position_mocap(vehicle_position, mocap, h_fig)
figure(h_fig)

subplot(311)
hold on
plot(mocap.timestamp, mocap.x);
plot(vehicle_position.timestamp, vehicle_position.x);
grid
ylabel('x [m]')
legend('mocap', 'estimate');
title('Position: estimate vs mocap')

subplot(312)
hold on
plot(mocap.timestamp, mocap.y);
plot(vehicle_position.timestamp, vehicle_position.y);
grid
ylabel('y [m]')
% legend('estimate', 'mocap');

subplot(313)
hold on
plot(mocap.timestamp, mocap.z);
plot(vehicle_position.timestamp, vehicle_position.z);
grid
ylabel('z [m]')
% legend('estimate', 'mocap');
xlabel('time [s]')

end

function plot_velocity_mocap(vehicle_position, mocap, h_fig, str_id_warning)

% compute numerical derivative of mocap position
ts = mean(diff(mocap.timestamp)); % mocap sampling period
tvec = mocap.timestamp(1):ts:mocap.timestamp(end); % uniform time grid

xi = interp1(mocap.timestamp, mocap.x, tvec);
yi = interp1(mocap.timestamp, mocap.y, tvec);
zi = interp1(mocap.timestamp, mocap.z, tvec);

Fd = tf([1 -1], [ts, 0], ts); % backward euler discrete-time derivative

% patch: remove the following warning
% Warning: Simulation will start at a nonzero initial time.
warning('off', str_id_warning);
vx_filt = lsim(Fd, xi, tvec);
vy_filt = lsim(Fd, yi, tvec);
vz_filt = lsim(Fd, zi, tvec);
warning('on', str_id_warning);

% remove effect of transient
vx_filt(1) = NaN;
vy_filt(1) = NaN;
vz_filt(1) = NaN;

figure(h_fig)

subplot(311)
hold on
plot(tvec, vx_filt);
plot(vehicle_position.timestamp, vehicle_position.vx);
grid
ylabel('vx [m/s]')
legend('mocap', 'estimate');
title('Velocity: estimate vs mocap')

subplot(312)
hold on
plot(tvec, vy_filt);
plot(vehicle_position.timestamp, vehicle_position.vy);
grid
ylabel('vy [m/s]')

subplot(313)
hold on
plot(tvec, vz_filt);
plot(vehicle_position.timestamp, vehicle_position.vz);
grid
ylabel('vz [m/s]')
xlabel('time [s]')

end

function plot_position_odom(vehicle_position, odom, h_fig)
figure(h_fig)

subplot(311)
hold on
plot(odom.timestamp, odom.x);
plot(vehicle_position.timestamp, vehicle_position.x);
grid
ylabel('x [m]')
legend('mocap', 'estimate');
title('Position: estimate vs odom')

subplot(312)
hold on
plot(odom.timestamp, odom.y);
plot(vehicle_position.timestamp, vehicle_position.y);
grid
ylabel('y [m]')
% legend('estimate', 'mocap');

subplot(313)
hold on
plot(odom.timestamp, odom.z);
plot(vehicle_position.timestamp, vehicle_position.z);
grid
ylabel('z [m]')
% legend('estimate', 'mocap');
xlabel('time [s]')

end

function plot_velocity_odom(vehicle_position, odom, h_fig, str_id_warning)

% compute numerical derivative of mocap position
ts = mean(diff(odom.timestamp)); % mocap sampling period
tvec = odom.timestamp(1):ts:odom.timestamp(end); % uniform time grid

xi = interp1(odom.timestamp, odom.x, tvec);
yi = interp1(odom.timestamp, odom.y, tvec);
zi = interp1(odom.timestamp, odom.z, tvec);

figure
plot(tvec, xi)

Fd = tf([1 -1], [ts, 0], ts); % backward euler discrete-time derivative

% patch: remove the following warning
% Warning: Simulation will start at a nonzero initial time.
warning('off', str_id_warning);
vx_filt = lsim(Fd, xi, tvec);
vy_filt = lsim(Fd, yi, tvec);
vz_filt = lsim(Fd, zi, tvec);
warning('on', str_id_warning);

% remove effect of transient
vx_filt(1) = NaN;
vy_filt(1) = NaN;
vz_filt(1) = NaN;

figure(h_fig)

subplot(311)
hold on
plot(tvec, vx_filt);
plot(vehicle_position.timestamp, vehicle_position.vx);
grid
ylabel('vx [m/s]')
legend('mocap', 'estimate');
title('Velocity: estimate vs odom')

subplot(312)
hold on
plot(tvec, vy_filt);
plot(vehicle_position.timestamp, vehicle_position.vy);
grid
ylabel('vy [m/s]')

subplot(313)
hold on
plot(tvec, vz_filt);
plot(vehicle_position.timestamp, vehicle_position.vz);
grid
ylabel('vz [m/s]')
xlabel('time [s]')

end

function plot_velocity_gps(vehicle_position, gps, h_fig)

figure(h_fig)
subplot(311)
plot(gps.timestamp, gps.vel_n_m_s);
hold on
plot(vehicle_position.timestamp, vehicle_position.vx)
grid
ylabel('vx [m/s]')
title('NED velocity')

subplot(312)
plot(gps.timestamp, gps.vel_e_m_s);
hold on
plot(vehicle_position.timestamp, vehicle_position.vy)
grid
ylabel('vy [m/s]')

subplot(313)
plot(gps.timestamp, gps.vel_d_m_s);
hold on
plot(vehicle_position.timestamp, vehicle_position.vz)
grid
ylabel('vz [m/s]')

legend('GPS', 'estimate')
xlabel('time [s]')

end

function plot_altitude_gps(sensor_combined, gps, global_position, h_fig)

figure(h_fig);

% plot(sensor_combined.timestamp, sensor_combined.baro_alt_meter)
% hold on
plot(global_position.timestamp, global_position.alt)
hold on
plot(gps.timestamp, gps.alt/1e3)
legend('vehicle global position', 'GPS')
% legend('barometer', 'vehicle global position', 'GPS')
grid
title('Vehicle altitude')
ylabel('[m]')

end

function plot_position_gps_local(gps, h_fig)

% transform GPS position from spherical coordinates to local frame
lat = gps.lat/1e7;
lon = gps.lon/1e7;
alt = gps.alt_ellipsoid/1e3;

lat0 = lat(1);
lon0 = lon(1);
alt0 = alt(1);

[xned, yned, zned] = geodetic2ned(lat, lon, alt, lat0, lon0, alt0, wgs84Ellipsoid);

% plot: raw GPS coordinates transformed to local frame
figure(h_fig);
plot(gps.timestamp, xned)
hold on
grid
plot(gps.timestamp, yned)
plot(gps.timestamp, zned)
xlabel('time [s]')
legend('N', 'E', 'D');
title('GPS position (local frame)')
ylabel('[m]')

end

function plot_position_gps_local_trajectory(gps, h_fig)

% transform GPS position from spherical coordinates to local frame
lat = gps.lat/1e7;
lon = gps.lon/1e7;
alt = gps.alt_ellipsoid/1e3;

lat0 = lat(1);
lon0 = lon(1);
alt0 = alt(1);

[xned, yned, zned] = geodetic2ned(lat, lon, alt, lat0, lon0, alt0, wgs84Ellipsoid);

% plot: raw GPS coordinates transformed to local frame, N-E trajectory
figure(h_fig);
plot(yned, xned)
hold on
hdots = plot(yned(1), xned(1), 'bs', ...
    yned(end), xned(end), 'ro');
axis equal
grid
xlabel('E [m]')
ylabel('N [m]')
title('In-plane trajectory (GPS, local frame)')
legend(hdots, {'start', 'end'})

end

function plot_position_gps_vs_local(gps, local_position, h_fig)

% transform GPS position from spherical coordinates to local frame
lat = gps.lat/1e7;
lon = gps.lon/1e7;
alt = gps.alt_ellipsoid/1e3;

lat0 = lat(1);
lon0 = lon(1);
alt0 = alt(1);

[xned, yned, zned] = geodetic2ned(lat, lon, alt, lat0, lon0, alt0, wgs84Ellipsoid);

% plot: raw GPS coordinates transformed to local frame vs local position
% estimate
figure(h_fig);
subplot(311)
plot(gps.timestamp, xned - xned(1))
hold on
plot(local_position.timestamp, local_position.x - local_position.x(1))
grid
title('Local position (starting from initial position)')
legend('GPS', 'estimate')
ylabel('N [m]')

subplot(312)
plot(gps.timestamp, yned - yned(1))
hold on
plot(local_position.timestamp, local_position.y - local_position.y(1))
grid
ylabel('E [m]')

subplot(313)
plot(gps.timestamp, zned - zned(1))
hold on
plot(local_position.timestamp, local_position.z - local_position.z(1))
grid
ylabel('D [m]')
xlabel('time [s]')

end

function plot_attitude_vs_setpoint(vehicle_attitude, vehicle_attitude_sp, h_fig)

eul_attitude = quat2eul(vehicle_attitude.q,'ZYX');
eul_attitude_setpoint = quat2eul(vehicle_attitude_sp.q_d,'ZYX');

figure(h_fig)

subplot(311)
plot(vehicle_attitude.timestamp, eul_attitude(:,3)/pi*180, 'b')
hold on
grid
plot(vehicle_attitude_sp.timestamp, eul_attitude_setpoint(:,3)/pi*180, 'r--')
ylabel('phi [deg]')
title('Attitude angles vs setpoints')

subplot(312)
plot(vehicle_attitude.timestamp, eul_attitude(:,2)/pi*180, 'b')
hold on
grid
plot(vehicle_attitude_sp.timestamp, eul_attitude_setpoint(:,2)/pi*180, 'r--')
ylabel('theta [deg]')

subplot(313)
plot(vehicle_attitude.timestamp, eul_attitude(:,1)/pi*180, 'b')
hold on
grid
plot(vehicle_attitude_sp.timestamp, eul_attitude_setpoint(:,1)/pi*180, 'r--')
ylabel('psi [deg]')
xlabel('time [s]')
legend({'angle', 'setpoint'})

end

function plot_attitude_vs_setpoint_custom(vehicle_attitude, vehicle_attitude_sp, h_fig)

eul_attitude = quat2eul(vehicle_attitude.q,'ZYX');
eul_attitude_setpoint = quat2eul(vehicle_attitude_sp.q_r,'ZYX');

figure(h_fig)

subplot(311)
plot(vehicle_attitude.timestamp, eul_attitude(:,3)/pi*180, 'b')
hold on
grid
plot(vehicle_attitude_sp.timestamp, eul_attitude_setpoint(:,3)/pi*180, 'r--')
ylabel('phi [deg]')
title('Attitude angles vs setpoints')

subplot(312)
plot(vehicle_attitude.timestamp, eul_attitude(:,2)/pi*180, 'b')
hold on
grid
plot(vehicle_attitude_sp.timestamp, eul_attitude_setpoint(:,2)/pi*180, 'r--')
ylabel('theta [deg]')

subplot(313)
plot(vehicle_attitude.timestamp, eul_attitude(:,1)/pi*180, 'b')
hold on
grid
plot(vehicle_attitude_sp.timestamp, eul_attitude_setpoint(:,1)/pi*180, 'r--')
ylabel('psi [deg]')
xlabel('time [s]')
legend({'angle', 'setpoint'})

end

function plot_rates(vehicle_angular_rates, h_fig)

figure(h_fig)

hold on
plot(vehicle_angular_rates.timestamp, vehicle_angular_rates.xyz(:,1)/pi*180)
plot(vehicle_angular_rates.timestamp, vehicle_angular_rates.xyz(:,2)/pi*180)
plot(vehicle_angular_rates.timestamp, vehicle_angular_rates.xyz(:,3)/pi*180)
grid
ylabel('[deg/s]')
legend({'p', 'q', 'r'})
title('Angular rates')
xlabel('time [s]')

end

function plot_rates_vs_setpoints(vehicle_angular_rates, vehicle_rate_sp, h_fig)

figure(h_fig);

subplot(311)
plot(vehicle_angular_rates.timestamp, vehicle_angular_rates.xyz(:,1)/pi*180)
hold on
plot(vehicle_rate_sp.timestamp, vehicle_rate_sp.roll/pi*180, '--')
grid
legend({'rate', 'setpoint'})
% xlabel('time [s]')
ylabel('p [deg/s]')
title('Angular rates vs setpoints')

subplot(312)
plot(vehicle_angular_rates.timestamp, vehicle_angular_rates.xyz(:,2)/pi*180)
hold on
plot(vehicle_rate_sp.timestamp, vehicle_rate_sp.pitch/pi*180, '--')
grid
% legend('rate', 'setpoint')
% xlabel('time [s]')
ylabel('q [deg/s]')

subplot(313)
plot(vehicle_angular_rates.timestamp, vehicle_angular_rates.xyz(:,3)/pi*180)
hold on
plot(vehicle_rate_sp.timestamp, vehicle_rate_sp.yaw/pi*180, '--')
grid
% legend('rate', 'setpoint')
xlabel('time [s]')
ylabel('r [deg/s]')

end

function plot_moments(control_moments, h_fig)

figure(h_fig)

hold on
plot(control_moments.timestamp, control_moments.control(:,1))
plot(control_moments.timestamp, control_moments.control(:,2))
plot(control_moments.timestamp, control_moments.control(:,3))
grid
title('Control actions')
xlabel('time [s]')
legend({'L', 'M', 'N'})

end

function plot_moments_custom(control_moments, h_fig)

figure(h_fig)

hold on
plot(control_moments.timestamp, control_moments.l)
plot(control_moments.timestamp, control_moments.m)
plot(control_moments.timestamp, control_moments.n)
grid
title('Control actions: normalized moments')
xlabel('time [s]')
legend({'L', 'M', 'N'})

end

function plot_forces_custom(control_forces, h_fig)

figure(h_fig)

hold on
plot(control_forces.timestamp, control_forces.fx)
plot(control_forces.timestamp, control_forces.fy)
plot(control_forces.timestamp, control_forces.fz)
grid

% compute force vector magnitude
F = sqrt(control_forces.fx.^2 + control_forces.fy.^2 + control_forces.fz.^2);
plot(control_forces.timestamp, F, 'k');

title('Control actions: normalized forces')
xlabel('time [s]')
legend({'Fx', 'Fy', 'Fz', 'total'})

end

function plot_thrust(control_moments, h_fig)

figure(h_fig);

thrust = control_moments.control(:,4);

THRESH_THRUST = 0.2;
flag_int_reset = thrust < THRESH_THRUST;
grid
hold on
plot(control_moments.timestamp, thrust)
stairs(control_moments.timestamp, flag_int_reset, 'r-')
title('Thrust')
xlabel('time [s]')
legend({'thrust', 'integral reset flag'})

end

function plot_thrust_custom(control_actions, flight_state, h_fig)

figure(h_fig);

thrust = control_actions.fz;

%%% reconstruct FLAG_RESET_INT based on state of internal flight_controller
%%% state machine
% _ flag reset integral is false if state == INFLIGHT
% _ otherwise, flag reset integral is true
FLIGHT_STATE_INFLIGHT = 2; % from state definitions in fw flight_controller module
flag_int_reset = flight_state.state ~= FLIGHT_STATE_INFLIGHT;

grid
hold on
plot(control_actions.timestamp, thrust)
stairs(flight_state.timestamp, double(flag_int_reset), 'r-')
title('Thrust')
xlabel('time [s]')
legend({'thrust', 'integral reset flag'})

end

function plot_motors_custom(motor_controls, h_fig)

figure(h_fig)

plot(motor_controls.timestamp, motor_controls.control);
grid
title('Motors normalized control actions')
xlabel('time [s]')
legend('1', '2','3','4','5','6','7','8');

ylim([-0.1, 1.1])
hold on
t = motor_controls.timestamp;
plot(t, 0*ones(size(t)), 'k--')
plot(t, 1*ones(size(t)), 'k--')

end

function plot_servos_custom(servo_controls, h_fig)

figure(h_fig)

N = 4; % max 4 servos supported
plot(servo_controls.timestamp, servo_controls.control(:, 1:N));
grid
title('Servos normalized control actions')
xlabel('time [s]')
legend('1', '2','3','4');

ylim([-1.2 1.2])
hold on
t = servo_controls.timestamp;
plot(t, -1*ones(size(t)), 'k--')
plot(t, 1*ones(size(t)), 'k--')
plot(t, 0*ones(size(t)), 'k--')

end

function plot_pwm(actuator_pwm, h_fig)

figure(h_fig)

N = 8;
plot(actuator_pwm.timestamp, actuator_pwm.output(:,1:N))
grid
xlabel('time [s]')
legend('1', '2','3','4','5','6','7','8');
title('Actuators PWM')
xlabel('time [s]')
ylabel('[PWM]')

end

function plot_position(position, h_fig)

figure(h_fig)

plot(position.timestamp, position.x, 'b', ...
    position.timestamp, position.y, 'r', ...
    position.timestamp, position.z, 'k')
grid
legend({'N', 'E', 'D'})
xlabel('time [s]')
ylabel('[m]')
title('Position')

end

function plot_velocity(velocity, h_fig)

figure(h_fig)

plot(velocity.timestamp, velocity.vx, 'b', ...
    velocity.timestamp, velocity.vy, 'r', ...
    velocity.timestamp, velocity.vz, 'k')
grid
legend({'V_N', 'V_E', 'V_D'})
xlabel('time [s]')
ylabel('[m/s]')
title('Velocity')

end

function plot_trajectory(position, h_fig)

figure(h_fig)

plot(position.y, position.x)
hold on
hdots = plot(position.y(1), position.x(1), 'bs', ...
    position.y(end), position.x(end), 'ro');
grid
axis equal
xlabel('E [m]')
ylabel('N [m]')
title('Trajectory')
legend(hdots, {'start', 'end'})

end

function plot_trajectory_3d(position, h_fig)

figure(h_fig)

plot3(position.y, position.x, -position.z)
hold on
hdots = plot3(position.y(1), position.x(1), -position.z(1), 'bs', ...
    position.y(end), position.x(end), -position.z(end), 'ro');
axis equal
% xlim([-2.5,2.5])
% ylim([-2.5,2.5])
% zlim([0,5])
xlabel('E [m]');
ylabel('N [m]');
zlabel('U [m]');
grid
title('3D trajectory')
legend(hdots, {'start', 'end'})

end

function plot_position_vs_setpoint(position, position_sp, h_fig)

figure(h_fig)

subplot(311)
plot(position.timestamp, position.x, 'b', ...
    position_sp.timestamp, position_sp.x, 'r--')
grid
ylabel('N [m]')
title('Position vs setpoint')
subplot(312)
plot(position.timestamp, position.y, 'b', ...
    position_sp.timestamp, position_sp.y, 'r--')
grid
ylabel('E [m]')
subplot(313)
plot(position.timestamp, position.z, 'b', ...
    position_sp.timestamp, position_sp.z, 'r--')
grid
ylabel('D [m]')
legend({'position', 'setpoint'})
xlabel('time [s]')

end

function plot_position_vs_setpoint_custom(position, position_sp, h_fig)

figure(h_fig)

subplot(311)
plot(position.timestamp, position.x, 'b', ...
    position_sp.timestamp, position_sp.p_r(:,1), 'r--')
grid
ylabel('N [m]')
title('Position vs setpoint')
subplot(312)
plot(position.timestamp, position.y, 'b', ...
    position_sp.timestamp, position_sp.p_r(:,2), 'r--')
grid
ylabel('E [m]')
subplot(313)
plot(position.timestamp, position.z, 'b', ...
    position_sp.timestamp, position_sp.p_r(:,3), 'r--')
grid
ylabel('D [m]')
legend({'position', 'setpoint'})
xlabel('time [s]')

end

function plot_velocity_vs_setpoint(position, position_sp, h_fig)

figure(h_fig)

subplot(311)
plot(position.timestamp, position.vx, 'b', ...
    position_sp.timestamp, position_sp.vx, 'r--')
grid
ylabel('V_N [m/s]')
title('Velocity vs setpoint')
subplot(312)
plot(position.timestamp, position.vy, 'b', ...
    position_sp.timestamp, position_sp.vy, 'r--')
grid
ylabel('V_E [m/s]')
subplot(313)
plot(position.timestamp, position.vz, 'b', ...
    position_sp.timestamp, position_sp.vz, 'r--')
grid
ylabel('V_D [m/s]')
legend({'velocity', 'setpoint'})
xlabel('time [s]')

end

function plot_velocity_vs_setpoint_custom(position, position_sp, h_fig)

figure(h_fig)

subplot(311)
plot(position.timestamp, position.vx, 'b', ...
    position_sp.timestamp, position_sp.v_r(:,1), 'r--')
grid
ylabel('V_N [m/s]')
title('Velocity vs setpoint')
subplot(312)
plot(position.timestamp, position.vy, 'b', ...
    position_sp.timestamp, position_sp.v_r(:,2), 'r--')
grid
ylabel('V_E [m/s]')
subplot(313)
plot(position.timestamp, position.vz, 'b', ...
    position_sp.timestamp, position_sp.v_r(:,3), 'r--')
grid
ylabel('V_D [m/s]')
legend({'velocity', 'setpoint'})
xlabel('time [s]')

end

function plot_body_velocity_vs_setpoint(position, position_sp, vehicle_attitude, h_fig)

velocity_ned = [ position.vx, position.vy, position.vz ];
velocity_ned_sp = [ position_sp.vx, position_sp.vy, position_sp.vz ];

velocity_ned_interp = interp1(position.timestamp, velocity_ned, vehicle_attitude.timestamp);
velocity_ned_sp_interp = interp1(position_sp.timestamp, velocity_ned_sp, vehicle_attitude.timestamp);

attitude = quat2dcm(vehicle_attitude.q);

%%% compute attitude matrix
N = size(attitude,3);
body_velocity_interp = zeros(3,N);
body_velocity_sp = zeros(3,N);
for i = 1:N
    body_velocity_interp(:,i) = attitude(:,:,i) * velocity_ned_interp(i,:)';
    body_velocity_sp (:,i) = attitude(:,:,i) * velocity_ned_sp_interp(i,:)';
end

figure(h_fig)

subplot(311)
plot(vehicle_attitude.timestamp, body_velocity_interp(1,:), 'b', ...
    vehicle_attitude.timestamp,body_velocity_sp(1,:), 'r--')
grid
ylabel('u [m/s]')
title('Body velocity vs setpoint')
subplot(312)
plot(vehicle_attitude.timestamp, body_velocity_interp(2,:), 'b', ...
    vehicle_attitude.timestamp, body_velocity_sp(2,:), 'r--')
grid
ylabel('v [m/s]')
subplot(313)
plot(vehicle_attitude.timestamp, body_velocity_interp(3,:), 'b', ...
    vehicle_attitude.timestamp, body_velocity_sp(3,:), 'r--')
grid
ylabel('w [m/s]')
legend({'velocity', 'setpoint'})
xlabel('time [s]')

end

function plot_body_velocity_vs_setpoint_custom(position, position_sp, vehicle_attitude, h_fig)

velocity_ned = [ position.vx, position.vy, position.vz ];
velocity_ned_sp = [ position_sp.v_r(:,1), position_sp.v_r(:,2), position_sp.v_r(:,3) ];

velocity_ned_interp = interp1(position.timestamp, velocity_ned, vehicle_attitude.timestamp);
velocity_ned_sp_interp = interp1(position_sp.timestamp, velocity_ned_sp, vehicle_attitude.timestamp);

attitude = quat2dcm(vehicle_attitude.q);

%%% compute attitude matrix
N = size(attitude,3);
body_velocity_interp = zeros(3,N);
body_velocity_sp = zeros(3,N);
for i = 1:N
    body_velocity_interp(:,i) = attitude(:,:,i) * velocity_ned_interp(i,:)';
    body_velocity_sp (:,i) = attitude(:,:,i) * velocity_ned_sp_interp(i,:)';
end

figure(h_fig)

subplot(311)
plot(vehicle_attitude.timestamp, body_velocity_interp(1,:), 'b', ...
    vehicle_attitude.timestamp,body_velocity_sp(1,:), 'r--')
grid
ylabel('u [m/s]')
title('Body velocity vs setpoint')
subplot(312)
plot(vehicle_attitude.timestamp, body_velocity_interp(2,:), 'b', ...
    vehicle_attitude.timestamp, body_velocity_sp(2,:), 'r--')
grid
ylabel('v [m/s]')
subplot(313)
plot(vehicle_attitude.timestamp, body_velocity_interp(3,:), 'b', ...
    vehicle_attitude.timestamp, body_velocity_sp(3,:), 'r--')
grid
ylabel('w [m/s]')
legend({'velocity', 'setpoint'})
xlabel('time [s]')

end

function plot_cruise_speed_vs_setpoint(position, position_sp, h_fig)

cruise_speed = sqrt(position.vx.^2 + position.vy.^2 + position.vz.^2);
cruise_speed_sp = sqrt(position_sp.vx.^2 + position_sp.vy.^2 + position_sp.vz.^2);

figure(h_fig)
plot(position.timestamp, cruise_speed, 'b', ...
    position_sp.timestamp, cruise_speed_sp, 'r--')
grid
ylabel('V [m/s]')
xlabel('time [s]')
title('Cruise-speed vs setpoint')
legend({'velocity', 'setpoint'})

end

function plot_cruise_speed_vs_setpoint_custom(position, position_sp, h_fig)

cruise_speed = sqrt(position.vx.^2 + position.vy.^2 + position.vz.^2);
cruise_speed_sp = sqrt(position_sp.v_r(:,1).^2 + position_sp.v_r(:,2).^2 + position_sp.v_r(:,3).^2);

figure(h_fig)
plot(position.timestamp, cruise_speed, 'b', ...
    position_sp.timestamp, cruise_speed_sp, 'r--')
grid
ylabel('V [m/s]')
xlabel('time [s]')
title('Cruise-speed vs setpoint')
legend({'velocity', 'setpoint'})

end

function plot_trajectory_vs_setpoint(position, position_sp, h_fig)

figure(h_fig)

htraj = plot(position.y, position.x, 'b');
hold on
hdots = plot(position.y(1), position.x(1), 'bs', ...
    position.y(end), position.x(end), 'ro');
hsetp = plot(position_sp.y, position_sp.x, 'r--');
grid
axis equal
xlabel('E [m]')
ylabel('N [m]')
title('Trajectory')
legend([htraj; hsetp; hdots], {'position', 'setpoint', 'start', 'end'})

end

function plot_trajectory_vs_setpoint_custom(position, position_sp, h_fig)

figure(h_fig)

htraj = plot(position.y, position.x, 'b');
hold on
hdots = plot(position.y(1), position.x(1), 'bs', ...
    position.y(end), position.x(end), 'ro');
hsetp = plot(position_sp.p_r(:,2), position_sp.p_r(:,1), 'r--');
grid
axis equal
xlabel('E [m]')
ylabel('N [m]')
title('Trajectory')
legend([htraj; hsetp; hdots], {'position', 'setpoint', 'start', 'end'})

end

function plot_trajectory_3d_vs_setpoint(position, position_sp, h_fig)

figure(h_fig)

htraj = plot3(position.y, position.x, -position.z);
hold on
hdots = plot3(position.y(1), position.x(1), -position.z(1), 'bs', ...
    position.y(end), position.x(end), -position.z(end), 'ro');
hsetp = plot3(position_sp.y, position_sp.x, -position_sp.z, 'r--');
axis equal
% xlim([-2.5,2.5])
% ylim([-2.5,2.5])
% zlim([0,5])
xlabel('E [m]');
ylabel('N [m]');
zlabel('U [m]');
grid
title('3D trajectory')
legend([htraj; hsetp; hdots], {'position', 'setpoint', 'start', 'end'})

end

function plot_trajectory_3d_vs_setpoint_custom(position, position_sp, h_fig)

figure(h_fig)

htraj = plot3(position.y, position.x, -position.z);
hold on
hdots = plot3(position.y(1), position.x(1), -position.z(1), 'bs', ...
    position.y(end), position.x(end), -position.z(end), 'ro');
hsetp = plot3(position_sp.p_r(:,2), position_sp.p_r(:,1), -position_sp.p_r(:,3), 'r--');
axis equal
% xlim([-2.5,2.5])
% ylim([-2.5,2.5])
% zlim([0,5])
xlabel('E [m]');
ylabel('N [m]');
zlabel('U [m]');
grid
title('3D trajectory')
legend([htraj; hsetp; hdots], {'position', 'setpoint', 'start', 'end'})

end

function plot_control_mode(control_mode, h_fig)

figure(h_fig)

stairs(control_mode.timestamp, control_mode.flag_control_attitude_enabled)
hold on
stairs(control_mode.timestamp, control_mode.flag_control_altitude_enabled)
stairs(control_mode.timestamp, control_mode.flag_control_position_enabled)
grid
legend({'attitude', 'altitude', 'position'})
title('Control mode flags')
xlabel('time [s]')
ylim([-0.1, 1.1])

end

function isLogged = isTopicLogged(loggedTopicNames, STR_NAME_TOPIC)
isLogged = false;

for i = 1:length(loggedTopicNames)
    ss = loggedTopicNames{i};
    if strcmp(STR_NAME_TOPIC, ss)
        isLogged = true;
    end
end

end

%% END OF CODE
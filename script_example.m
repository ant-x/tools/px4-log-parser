%SCRIPT_EXAMPLE Example script of usage of ulg2mat() and
%plot_logged_data_px4()
% 
% Authors
% Simone Panza <simone@antx.it>
% Mattia Giurato <mattia@antx.it>
% ANT-X s.r.l.
% Date 2024/01/03
%
% Changelog:
%   v2
%       add example with mask
%   v1
%       first version

%% EXAMPLE#1: BASIC USAGE
% Scenario: I have a log file log_2_2024-1-18-14-59-14.ulg and I want to 
% quickly plot its content.
% The file is located in folder $[HOME_PARSER]/ulg_logs

clearvars
close all
clc

ulg_file_name = 'log_2_2024-1-18-14-59-14'; % file name without .ulg extension
returned_parsed_logfile_path = ulg2mat(ulg_file_name); % the path of the parsed logfile is returned as an output argument
plotLoggedDataPx4(returned_parsed_logfile_path) % plot

clear ulg_file_name;

%% EXAMPLE#1_bis: LIMIT PLOTS
% Plot only quantities related to attitude and control actions
% The file is located in folder $[HOME_PARSER]/ulg_logs
clearvars
close all
clc

ulg_file_name = 'log_2_2024-1-18-14-59-14'; % file name without .ulg extension
parsed_logfile_path = strcat(getLogDir, filesep, ulg_file_name);
plotLoggedDataPx4(parsed_logfile_path, 'AC')

clear ulg_file_name;

%% EXAMPLE#2: ADVANCED USAGE
% Scenario: I have a logfile inside a subdirectory of the ulg log
% directory. The subdirectory has format YYYY-MM-DD.
% I want to parse the ulg file and create a corresponding directory in the
% parsed log directory.
% The file is located in folder $[HOME_PARSER]/ulg_logs/YYYY-MM-DD/hh-mm-ss.ulg

clearvars
close all
clc

input_file_name = '14-57-58'; % file name without .ulg extension
log_date = '2024-1-18'; % name of the folder containing the ulg file
output_file_name = strcat(erase(log_date,'-'),'_',erase(input_file_name,'-'));

raw_folder = fullfile(pwd, 'ulg_logs', log_date); % path of directory containing ulg file
parsed_dir = fullfile(pwd, 'mat_logs'); % path of directory which shall contain the parsed file

parsed_path = ulg2mat(input_file_name, 'RawLogFileDir', raw_folder, 'MatLogFileDir', parsed_dir, 'OutputFileName', output_file_name);

plotLoggedDataPx4(parsed_path);

clear input_file_name log_date output_file_name raw_folder parsed_dir;

%% EXAMPLE#3: ANT-X 2DoF Drone
% Scenario: I have a log file log_2_2024-1-18-14-59-14.ulg and I want to 
% quickly plot its content.
% The file is located in folder $[HOME_PARSER]/ulg_logs

clearvars
close all
clc

input_file_name = 'log_2_2024-1-18-14-59-14'; % file name without .ulg extension

parsed_path = twoDofLogConverter(input_file_name);

plotLoggedDataTwoDof(parsed_path);

clear input_file_name;

%% EXAMPLE#3bis: ANT-X 2DoF Drone
% Scenario: I have a logfile inside a subdirectory of the ulg log
% directory. The subdirectory has format YYYY-MM-DD.
% I want to parse the ulg file and create a corresponding directory in the
% parsed log directory.
% The file is located in folder $[HOME_PARSER]/ulg_logs/YYYY-MM-DD/hh-mm-ss.ulg

clearvars
close all
clc

input_file_name = '14-57-58'; % file name without .ulg extension
log_date = '2024-1-18'; % name of the folder containing the ulg file
output_file_name = strcat(erase(log_date,'-'),'_',erase(input_file_name,'-'), '_2dd');

input_folder = fullfile(pwd, 'ulg_logs', log_date); % path of directory containing ulg file

parsed_path = twoDofLogConverter(input_file_name, 'InputLogFileDir', input_folder, 'OutputFileName', output_file_name, 'Downsample', 'true');

plotLoggedDataTwoDof(parsed_path);

clear input_file_name log_date input_folder output_file_name;

%% END OF CODE
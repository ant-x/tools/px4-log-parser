function outputFileName = DroneLabLogConverter( ulgFileName, varargin )
%DRONELABLOGCONVERTER Extract data from ULOG file for the ANT-X Drone.
%
% This routine takes an input the .ulg file containing logged topics of a
% Dronelab Drone.
%
% INPUT
%   ulgFileName     a string containing the name of the .ulg file to be
%                   parsed (without extension)
%
% Additional input parameters
%   'InputLogFileDir'       directory containing the input .mat file
%   'OutputLogFileDir'      directory containing the output .mat file
%   'OutputFileName'        name of output .mat file
%   'defaultDownsample'     downsample the log data to 100 Hz
%
% OUTPUT
%   outputFileName  string of the name of .mat file containing results of
%                   parsing

%% INPUT PARSING
pars = inputParser;

containsNoSpaces = @(x) isempty(regexp(x, '\s', 'once')); % true if string x does not contain any whitespaces

checkFileName = @(x) ischar(x) & containsNoSpaces(x);

curDir = getCurrentDirectory();
defaultInputLogFileDir = fullfile(curDir, 'ulg_logs');
defaultOutputLogFileDir = fullfile(curDir, 'mat_logs');
defaultOutputFileName = strcat(ulgFileName,'_DL');
defaultDownsample = 'false';

addRequired(pars, 'ulgFileName', checkFileName);
addParameter(pars, 'InputLogFileDir', defaultInputLogFileDir, checkFileName);
addParameter(pars, 'OutputLogFileDir', defaultOutputLogFileDir, checkFileName);
addParameter(pars, 'OutputFileName', defaultOutputFileName, checkFileName);
addParameter(pars, 'Downsample', defaultDownsample, checkFileName);

parse(pars, ulgFileName, varargin{:});

input_dir = pars.Results.InputLogFileDir;
output_dir = pars.Results.OutputLogFileDir;
out_file = pars.Results.OutputFileName;
downsample = pars.Results.Downsample;

if strcmp(downsample, 'true')
    fprintf('\nData will be downsampled to 100 Hz\n');
end

%% parse ulg file
outputFileName = ulg2mat(ulgFileName, 'RawLogFileDir', input_dir, 'MatLogFileDir', output_dir, 'OutputFileName', out_file);

%% load logged data
input_log = load(outputFileName);

%% parse data
% sensors
try
    parsedData.acc.timestamp = input_log.sensor_combined_0.timestamp;
    parsedData.acc.value = input_log.sensor_combined_0.accelerometer_m_s2;
catch
end

try
    parsedData.gyr.timestamp = input_log.sensor_combined_0.timestamp;
    parsedData.gyr.value = input_log.sensor_combined_0.gyro_rad;
catch
end

% states
try
    parsedData.rate.timestamp = input_log.vehicle_angular_velocity_0.timestamp;
    parsedData.rate.value = input_log.vehicle_angular_velocity_0.xyz;
catch
end

try
    parsedData.att.timestamp = input_log.vehicle_attitude_0.timestamp;
    eul_attitude = quat2eul(input_log.vehicle_attitude_0.q,'ZYX');
    parsedData.att.value = eul_attitude;
    clear eul_attitude;
catch
end

try
    parsedData.quat.timestamp = input_log.vehicle_attitude_0.timestamp;
    parsedData.quat.value = input_log.vehicle_attitude_0.q;
catch
end

try
    parsedData.vel.timestamp = input_log.vehicle_local_position_0.timestamp;
    parsedData.vel.value(:,1) = input_log.vehicle_local_position_0.vx;
    parsedData.vel.value(:,2) = input_log.vehicle_local_position_0.vy;
    parsedData.vel.value(:,3) = input_log.vehicle_local_position_0.vz;
catch
end

try
    parsedData.pos.timestamp = input_log.vehicle_local_position_0.timestamp;
    parsedData.pos.value(:,1) = input_log.vehicle_local_position_0.x;
    parsedData.pos.value(:,2) = input_log.vehicle_local_position_0.y;
    parsedData.pos.value(:,3) = input_log.vehicle_local_position_0.z;
catch
end

try
    parsedData.mocap.timestamp = input_log.vehicle_visual_odometry_0.timestamp;
    parsedData.mocap.pos(:,1) = input_log.vehicle_visual_odometry_0.x;
    parsedData.mocap.pos(:,2) = input_log.vehicle_visual_odometry_0.y;
    parsedData.mocap.pos(:,3) = input_log.vehicle_visual_odometry_0.z;
    parsedData.mocap.quat = input_log.vehicle_visual_odometry_0.q;
catch
end

try
    parsedData.pos.timestamp = input_log.vehicle_local_position_0.timestamp;
    parsedData.pos.value(:,1) = input_log.vehicle_local_position_0.x;
    parsedData.pos.value(:,2) = input_log.vehicle_local_position_0.y;
    parsedData.pos.value(:,3) = input_log.vehicle_local_position_0.z;
catch
end

% feedforward
try
    MASK_EN_L = uint8(1);
    MASK_EN_M = uint8(2);
    MASK_EN_N = uint8(4);
    MASK_EN_Fx = uint8(8);
    MASK_EN_Fy = uint8(16);
    MASK_EN_Fz = uint8(32);
    
    parsedData.lmnf.timestamp = input_log.lmnf_injection_0.timestamp;
    parsedData.lmnf.L = input_log.lmnf_injection_0.l;
    parsedData.lmnf.M = input_log.lmnf_injection_0.m;
    parsedData.lmnf.N = input_log.lmnf_injection_0.n;
    parsedData.lmnf.Fx = input_log.lmnf_injection_0.f(:,1);
    parsedData.lmnf.Fy = input_log.lmnf_injection_0.f(:,2);
    parsedData.lmnf.Fz = input_log.lmnf_injection_0.f(:,3);
    parsedData.lmnf.openloop = boolean(input_log.lmnf_injection_0.type_openloop);
    parsedData.lmnf.enableL = bitand(uint8(input_log.lmnf_injection_0.mask),MASK_EN_L)>0;
    parsedData.lmnf.enableM = bitand(uint8(input_log.lmnf_injection_0.mask),MASK_EN_M)>0;
    parsedData.lmnf.enableN = bitand(uint8(input_log.lmnf_injection_0.mask),MASK_EN_N)>0;
    parsedData.lmnf.enableFx = bitand(uint8(input_log.lmnf_injection_0.mask),MASK_EN_Fx)>0;
    parsedData.lmnf.enableFy = bitand(uint8(input_log.lmnf_injection_0.mask),MASK_EN_Fy)>0;
    parsedData.lmnf.enableFz = bitand(uint8(input_log.lmnf_injection_0.mask),MASK_EN_Fz)>0;
catch
end

% setpoints
try
    parsedData.rate0.timestamp = input_log.flight_controller_internal_0.timestamp;
    parsedData.rate0.value = input_log.flight_controller_internal_0.omega_r;
catch
end

try
    parsedData.att0.timestamp = input_log.flight_controller_internal_0.timestamp;
    eul_attitude_sp = quat2eul(input_log.flight_controller_internal_0.q_r,'ZYX');
    parsedData.att0.value = eul_attitude_sp;
    clear eul_attitude_sp;
catch
end

try
    parsedData.quat0.timestamp = input_log.flight_controller_internal_0.timestamp;
    parsedData.quat0.value = input_log.flight_controller_internal_0.q_r;
catch
end

try
    parsedData.vel0.timestamp = input_log.flight_controller_internal_0.timestamp;
    parsedData.vel0.value = input_log.flight_controller_internal_0.v_r;
catch
end

try
    parsedData.pos0.timestamp = input_log.flight_controller_internal_0.timestamp;
    parsedData.pos0.value = input_log.flight_controller_internal_0.p_r;
catch
end

% control actions
try
    parsedData.mom.timestamp = input_log.actuator_controls_2_0.timestamp;
    parsedData.mom.value = input_log.actuator_controls_2_0.control(:,1:3);
catch
end

try
    parsedData.force.timestamp = input_log.actuator_controls_2_0.timestamp;
    parsedData.force.value = input_log.actuator_controls_2_0.control(:,4:6);
catch
end

if strcmp(downsample, 'true')    
    %% interpolate data
    % find initial time instant
    sample_time = 1/100;
    
    % sensors
    try
        time_vector = (parsedData.acc.timestamp(1):sample_time:parsedData.acc.timestamp(end))';
        parsedData.acc.value = interp1(parsedData.acc.timestamp, parsedData.acc.value, time_vector, 'previous');
        parsedData.acc.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.gyr.timestamp(1):sample_time:parsedData.gyr.timestamp(end))';
        parsedData.gyr.value = interp1(parsedData.gyr.timestamp, parsedData.gyr.value, time_vector, 'previous');
        parsedData.gyr.timestamp = time_vector;
    catch
    end
    
    % states
    try
        time_vector = (parsedData.rate.timestamp(1):sample_time:parsedData.rate.timestamp(end))';
        parsedData.rate.value = interp1(parsedData.rate.timestamp, parsedData.rate.value, time_vector, 'previous');
        parsedData.rate.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.att.timestamp(1):sample_time:parsedData.att.timestamp(end))';
        parsedData.att.value = interp1(parsedData.att.timestamp, parsedData.att.value, time_vector, 'previous');
        parsedData.att.timestamp = time_vector;
    catch
    end

    try
        time_vector = (parsedData.quat.timestamp(1):sample_time:parsedData.quat.timestamp(end))';
        parsedData.quat.value = interp1(parsedData.quat.timestamp, parsedData.quat.value, time_vector, 'previous');
        parsedData.quat.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.vel.timestamp(1):sample_time:parsedData.vel.timestamp(end))';
        parsedData.vel.value = interp1(parsedData.vel.timestamp, parsedData.vel.value, time_vector, 'previous');
        parsedData.vel.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.pos.timestamp(1):sample_time:parsedData.pos.timestamp(end))';
        parsedData.pos.value = interp1(parsedData.pos.timestamp, parsedData.pos.value, time_vector, 'previous');
        parsedData.pos.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.mocap.timestamp(1):sample_time:parsedData.mocap.timestamp(end))';
        parsedData.mocap.pos = interp1(parsedData.mocap.timestamp, parsedData.mocap.pos, time_vector, 'previous');
        parsedData.mocap.quat = interp1(parsedData.mocap.timestamp, parsedData.mocap.quat, time_vector, 'previous');
        parsedData.mocap.timestamp = time_vector;
    catch
        
    end
    % feedforward
  
    try
        time_vector = (parsedData.lmnf.timestamp(1):sample_time:parsedData.lmnf.timestamp(end))';
        parsedData.lmnf.L = interp1(parsedData.lmnf.timestamp, parsedData.lmnf.L, time_vector, 'previous');
        parsedData.lmnf.M = interp1(parsedData.lmnf.timestamp, parsedData.lmnf.M, time_vector, 'previous');
        parsedData.lmnf.N = interp1(parsedData.lmnf.timestamp, parsedData.lmnf.N, time_vector, 'previous');
        parsedData.lmnf.Fx = interp1(parsedData.lmnf.timestamp, parsedData.lmnf.Fx, time_vector, 'previous');
        parsedData.lmnf.Fy = interp1(parsedData.lmnf.timestamp, parsedData.lmnf.Fy, time_vector, 'previous');
        parsedData.lmnf.Fz = interp1(parsedData.lmnf.timestamp, parsedData.lmnf.Fz, time_vector, 'previous');
        parsedData.lmnf.openloop = interp1(parsedData.lmnf.timestamp, double(parsedData.lmnf.openloop), time_vector, 'previous');
        parsedData.lmnf.enableL = interp1(parsedData.lmnf.timestamp, double(parsedData.lmnf.enableL), time_vector, 'previous');
        parsedData.lmnf.enableM = interp1(parsedData.lmnf.timestamp, double(parsedData.lmnf.enableM), time_vector, 'previous');
        parsedData.lmnf.enableN = interp1(parsedData.lmnf.timestamp, double(parsedData.lmnf.enableN), time_vector, 'previous');
        parsedData.lmnf.enableFx = interp1(parsedData.lmnf.timestamp, double(parsedData.lmnf.enableFx), time_vector, 'previous');
        parsedData.lmnf.enableFy = interp1(parsedData.lmnf.timestamp, double(parsedData.lmnf.enableFy), time_vector, 'previous');
        parsedData.lmnf.enableFz = interp1(parsedData.lmnf.timestamp, double(parsedData.lmnf.enableFz), time_vector, 'previous');
        parsedData.lmnf.timestamp = time_vector;
    catch
    end
    
    % setpoints
    try
        time_vector = (parsedData.rate0.timestamp(1):sample_time:parsedData.rate0.timestamp(end))';
        parsedData.rate0.value = interp1(parsedData.rate0.timestamp, parsedData.rate0.value, time_vector, 'previous');
        parsedData.rate0.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.att0.timestamp(1):sample_time:parsedData.att0.timestamp(end))';
        parsedData.att0.value = interp1(parsedData.att0.timestamp, parsedData.att0.value, time_vector, 'previous');
        parsedData.att0.timestamp = time_vector;
    catch
    end
      
    try
        time_vector = (parsedData.quat0.timestamp(1):sample_time:parsedData.quat0.timestamp(end))';
        parsedData.quat0.value = interp1(parsedData.quat0.timestamp, parsedData.quat0.value, time_vector, 'previous');
        parsedData.quat0.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.vel0.timestamp(1):sample_time:parsedData.vel0.timestamp(end))';
        parsedData.vel0.value = interp1(parsedData.vel0.timestamp, parsedData.vel0.value, time_vector, 'previous');
        parsedData.vel0.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.pos0.timestamp(1):sample_time:parsedData.pos0.timestamp(end))';
        parsedData.pos0.value = interp1(parsedData.pos0.timestamp, parsedData.pos0.value, time_vector, 'previous');
        parsedData.pos0.timestamp = time_vector;
    catch
    end
    
    % control actions
    try
        time_vector = (parsedData.mom.timestamp(1):sample_time:parsedData.mom.timestamp(end))';
        parsedData.mom.value = interp1(parsedData.mom.timestamp, parsedData.mom.value, time_vector, 'previous');
        parsedData.mom.timestamp = time_vector;
    catch
    end
    
    try
        time_vector = (parsedData.force.timestamp(1):sample_time:parsedData.force.timestamp(end))';
        parsedData.force.value = interp1(parsedData.force.timestamp, parsedData.force.value, time_vector, 'previous');
        parsedData.force.timestamp = time_vector;
    catch
    end   
end

%% save to .mat file
if exist(output_dir, 'dir') == 0
    mkdir(output_dir)
end

outputFileName = fullfile(output_dir, sprintf('%s.mat', out_file));

if exist(outputFileName, 'file') == 2
    delete(outputFileName)
end

save(outputFileName, '-struct', 'parsedData');

end

function curDir = getCurrentDirectory()
% GETCURRENTDIRECTORY returns the path in the filesystem of this file

path_routine = mfilename('fullpath'); % get full path name of the current routine
PATH_DEPTH = 0; % the default dirs are located at the same level of this routine
% (e.g., PATH_DEPTH = 1 means a subdirectory wrt the current level)

ind_backslash_vec = regexp(path_routine, filesep);
ind_backslash = ind_backslash_vec(end - PATH_DEPTH);

curDir = path_routine(1:ind_backslash-1);

end

%% END OF CODE
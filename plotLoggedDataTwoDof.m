function plotLoggedDataTwoDof( str_fileName )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%% load logged data
r = load(str_fileName);

%% Plot angular rate
figure
hold on
plot(r.q.timestamp, r.q.value)
plot(r.q0.timestamp, r.q0.value)
hold off
grid on
box on
xlabel('time [s]')
ylabel('\omega [rad/s]')
legend('measure','setpoint')
title('Angular rate')

%% Plot attitude
figure
hold on
plot(r.theta.timestamp, r.theta.value)
plot(r.theta0.timestamp, r.theta0.value)
hold off
grid on
box on
xlabel('time [s]')
ylabel('\theta [rad]')
legend('measure','setpoint')
title('Attitude')

%% Plot velocity
figure
hold on
plot(r.v.timestamp, r.v.value)
plot(r.v0.timestamp, r.v0.value)
hold off
grid on
box on
xlabel('time [s]')
ylabel('v [m/s]')
legend('measure','setpoint')
title('Velocity')

%% Plot position
figure
hold on
plot(r.x.timestamp, r.x.value)
plot(r.x0.timestamp, r.x0.value)
hold off
grid on
box on
xlabel('time [s]')
ylabel('x [m/s]')
legend('measure','setpoint')
title('Position')

%% Plot Control actions
figure
subplot(2,1,1)
plot(r.M.timestamp, r.M.value)
grid on
box on
ylabel('Torque [1]')
subplot(2,1,2)
plot(r.T.timestamp, r.T.value)
grid on
box on
xlabel('time [s]')
ylabel('Thrust [1]')

%% Sensors
figure
subplot(3,1,1)
plot(r.acc.timestamp, r.acc.value)
title('Control actions')
grid on
box on
ylabel('Acc [m/s]')
title('Sensors')
subplot(3,1,2)
plot(r.gyr.timestamp, r.gyr.value)
grid on
box on
ylabel('Gyro [rad/s]')
subplot(3,1,3)
plot(r.dist.timestamp, r.dist.value)
grid on
box on
xlabel('time [s]')
ylabel('Distance [m]')

end
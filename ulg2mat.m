function outputFileName = ulg2mat( ulgFileName, varargin )
%ULG2MAT Extract data from ULOG file into .mat file
%
% ulg2mat(ulgFileName) parses the content of ulgFileName.ulg and saves the
% result in ulgFileName.mat. The .ulg file must be contained in the
% ulg_logs directory and the .mat file is saved in the mat_logs directory.
% 
% ulg2mat(ulgFileName, PlotData) plots the log content after parsing if the
% boolean PlotData is true (default: false).
% 
% INPUT
%   ulgFileName     a string containing the name of the .ulg file to be
%                   parsed (without extension)
%
% Additional input parameters
%   'PlotData'          boolean to enable plotting of the logged data
%                       (default false)
%   'RawLogFileDir'     directory containing the raw (.ulg) file
%   'MatLogFileDir'     output directory
%   'OutputFileName'    name of output .mat file
%   'ScaleTimeVec'      boolean to enable timestamp vector scaling and
%                       shifting (default true).
%   'RemoveRawDebugTopics'  remove from list the two topics
%                           debug_fc_signal_names and 
%                           debug_fc_signal_values (default true).
%   'ExternalDebugSignalNames'  import the topic debug_fc_signal_names from
%                               another parsed .mat file. this is useful if
%                               for some reason the log does not contain
%                               that topic (e.g., it was dropped) but it
%                               has been logged in another log.
% 
% OUTPUT
%   outputFileName  string of the name of .mat file containing results of
%                   parsing
% 
% Example:
%   ulg2mat('test') % parses the test.ulg file in the ulg_logs directory
%
% Usage of the 'ExternalDebugSignalNames' parameter:
% 
% % first parse a log 'A.ulg' containing the debug_fc_signal_names topic
% ulg2mat('A', 'RemoveRawDebugTopics', false);
% s = fullfile(getLogDir, 'A.mat')
% 
% % then pass its name to import it in the parsed 'B.ulg' log
% ulg2mat('B', 'ExternalDebugSignalNames', s)
%
% Authors
% Simone Panza <simone@antx.it>
% Mattia Giurato <mattia@antx.it>
% ANT-X s.r.l.
% Date 2024/01/03
%
% Changelog:
%   v17
%       use ulog_info to extract information from log file
%   v16
%       add new parameter ExternalDebugSignalNames to import the
%       debug_fc_signal_names topic from another parsed log.
%       this is useful if that topic has been dropped and not logged.
%   v15
%       implemented processing for custom debug topics
%       Simulink custom signals names and values can be logged in topics
%           debug_fc_signal_names
%           debug_fc_signal_values
%       During parsing, these two topics if present are fused in the new
%       topic debug_fc_signals
%   v14
%       fix: warning ID for readtable() (see issue#1) has changed from
%       'MATLAB:table:ModifiedVarnames' to
%       'MATLAB:table:ModifiedAndSavedVarnames' starting from release
%       R2017a.
%   v13
%       fix issue#1.
% 
%       a warning would occur in case the parsed csv file contains in the
%       top row variable names containing special characters (e.g.,
%       brackets []). this happens if the topic contains vector variables.
%       SOLUTION: the warning is disabled and re-enabled just after calling
%       readtable().
%   v12
%       add feedback in the command line during parsing
%   v11
%       updated doc
%   v10
%       fix bug: parsing would fail if no topics are contained in the log.
%       added warning 
%   v9
%       nice code re-format
%   v8
%       implement time vector scaling and shifting
%   v7
%       fix bug
%   v6
%       add issues section
%   v5
%       the temp directory is now located within the PX4 log parser dir
%       it is now possible to use the routine from any location of the
%       filesystem using the default raw and parsed log dirs
%   v4
%       change default dirs path
%   v3
%       introduce input parsing
%   v2
%       implement parsing on vector variables
%   v1
%       first version

% TODO 
% optional input args:
%  list of topics

% ISSUES
% 
% [SOLVED] issue#1: warning when topic contains vector variables.
% For each topic, the name of variables contained in the topic are stated
% in the first row of the corresponding `.csv` file. Vector variable names
% are saved as `varName[i]`. The `readtable()` routine will cast a warning
% whenever a vector variable is found in one of the topics:
%
% ``` Warning: Variable names were modified to make them valid MATLAB
% identifiers. ```
%
% This is due to the fact that `readtable()` converts the `varName[i]`
% variable name string as `varName_i_`. This warning can be ignored.

%%% DEFINE CONSTANTS

STR_NAME_TOPIC_DEBUG_FC_SIGNAL_NAMES = 'debug_fc_signal_names_0';
STR_NAME_TOPIC_DEBUG_FC_SIGNAL_VALUES = 'debug_fc_signal_values_0';
STR_NAME_TOPIC_DEBUG_FC_SIGNALS = 'debug_fc_signals_0';

%%% INPUT PARSING

pars = inputParser;

containsNoSpaces = @(x) isempty(regexp(x, '\s', 'once')); % true if string x does not contain any whitespaces

checkFileName = @(x) ischar(x) & containsNoSpaces(x);

curDir = getCurrentDirectory();
defaultRawLogFileDir = fullfile(curDir, 'ulg_logs');
defaultMatLogFileDir = fullfile(curDir, 'mat_logs');
defaultOutputFileName = ulgFileName;

checkScaleTimeVec = @(x) islogical(x);
defaultScaleTimeVec = true;

checkPlotData = @(x) islogical(x);
defaultPlotData = false;

checkRemoveRawDebug = @(x) islogical(x);
defaultRemoveRawDebug = true;

checkExternalDebugSignalNames = @(x) ischar(x);
defaultExternalDebugSignalNames = '';

addRequired(pars, 'ulgFileName', checkFileName);
addOptional(pars, 'PlotData', defaultPlotData, checkPlotData);
addParameter(pars, 'RawLogFileDir', defaultRawLogFileDir, checkFileName);
addParameter(pars, 'MatLogFileDir', defaultMatLogFileDir, checkFileName);
addParameter(pars, 'OutputFileName', defaultOutputFileName, checkFileName);
addParameter(pars, 'ScaleTimeVec', defaultScaleTimeVec, checkScaleTimeVec);
addParameter(pars, 'RemoveRawDebugTopics', defaultRemoveRawDebug, checkRemoveRawDebug);
addParameter(pars, 'ExternalDebugSignalNames', defaultExternalDebugSignalNames, checkExternalDebugSignalNames);

parse(pars, ulgFileName, varargin{:});

plot_data = pars.Results.PlotData;

dir_raw = pars.Results.RawLogFileDir;
dir_parsed = pars.Results.MatLogFileDir;
out_file = pars.Results.OutputFileName;

FLAG_SCALE_TIME_VEC = pars.Results.ScaleTimeVec;
FLAG_REMOVE_RAW_DEBUG = pars.Results.RemoveRawDebugTopics;

FLAG_EXT_DEBUG_NAMES = false;
topic_ext_debug_signal_names = struct([]);
file_ext_signal_names = pars.Results.ExternalDebugSignalNames;
if ~isempty(file_ext_signal_names)
    FLAG_EXT_DEBUG_NAMES = true;
    r = load(file_ext_signal_names);
    topic_ext_debug_signal_names = r.(STR_NAME_TOPIC_DEBUG_FC_SIGNAL_NAMES);
end

%%% END INPUT PARSING

% check MATLAB version is previous than R2017a
% reference for MATLAB release number: MATLAB release notes PDF documentation
% https://it.mathworks.com/help/pdf_doc/matlab/rn.pdf
if verLessThan('matlab', '9.2.0')
    STR_ID_WARNING_READTABLE = 'MATLAB:table:ModifiedVarnames'; % valid up to R2016b
else
    STR_ID_WARNING_READTABLE = 'MATLAB:table:ModifiedAndSavedVarnames'; % starting from R2017a
end

TMP_CSV_DIR = fullfile(curDir, 'tmp_csv_dir');
LOG_TYPE = '.ulg';

fprintf('\n\nULG log parser\n\n');
fprintf('Source directory:\t%s\n', dir_raw);
fprintf('Destination directory:\t%s\n', dir_parsed);
fprintf('Temp directory:\t%s\n\n', TMP_CSV_DIR);

logFileName = fullfile(dir_raw, [ulgFileName, LOG_TYPE]);

logExists = exist(logFileName, 'file') > 0;
assert(logExists, sprintf('File %s not found.', logFileName));
fprintf('Found .ulg file:\t%s\n\n', logFileName);

% extract log info with ulog_info command
fprintf('Extracting log info with ulog_info...\n\n');
commandInfoStr = sprintf('ulog_info %s', logFileName);

[status, commandOut] = system(commandInfoStr);

if status == 0
    fprintf(commandOut)
else
    error(commandOut)
end

%%% create temp folder to contain csv files
if exist(TMP_CSV_DIR, 'dir') == 0
    mkdir(TMP_CSV_DIR)
else
    fprintf('Temp directory nonempty. Cleaning up... ')
    rmdir(TMP_CSV_DIR,'s'); % cleanup
    mkdir(TMP_CSV_DIR);
    fprintf('Done.\n\n')
%     delete(fullfile(TMP_CSV_DIR, '*.csv')); % cleanup only .csv files    
end

%%% convert ulg to csv with the python script ulog2csv.py
fprintf('\nStart parsing the .ulg file with ulog2csv. Please wait...\n');
commandStr = sprintf('ulog2csv -o %s %s', TMP_CSV_DIR, logFileName);

[status, commandOut] = system(commandStr);

if status ~= 0
    error(commandOut)
end
fprintf('Parsing with ulog2csv done.\n\n')

%%% parse csv files and stack into struct
%csvFileNames = dir(sprintf('%s/*.csv', TMP_CSV_DIR));
csvFileNames = dir(fullfile(TMP_CSV_DIR, '*.csv'));

N = length(csvFileNames);

if N == 0
    warning('No topics found within log file!');
    outputFileName = '';
else
    
    parsedData = struct([]);
    
    for i = 1:N
        nam = fullfile(TMP_CSV_DIR, csvFileNames(i).name);
        [topicName, s] = importCsvData(ulgFileName, nam, STR_ID_WARNING_READTABLE);
        parsedData(1).(topicName) = s;
        fprintf('Found topic: %s\n', topicName);
    end
    
    rmdir(TMP_CSV_DIR,'s'); % cleanup
    
    %%% fuse debug signal names and values into a single topic
    %%% debug_fc_signals
    loggedTopicNames = fieldnames(parsedData);
    
    if isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_DEBUG_FC_SIGNAL_VALUES) && ...
            (isTopicLogged(loggedTopicNames, STR_NAME_TOPIC_DEBUG_FC_SIGNAL_NAMES) || FLAG_EXT_DEBUG_NAMES)
        
        fprintf('\nFound the following custom debug topics:\n\t%s\n\t%s\n', ...
            STR_NAME_TOPIC_DEBUG_FC_SIGNAL_NAMES, STR_NAME_TOPIC_DEBUG_FC_SIGNAL_VALUES);
        
        fprintf('which will be fused into the following new topic:\n\t%s\n\n', ...
            STR_NAME_TOPIC_DEBUG_FC_SIGNALS);
        
        if FLAG_EXT_DEBUG_NAMES % take names from external log
            topic_debug_signal_names = topic_ext_debug_signal_names;
        else % take the topic contained in current log
            topic_debug_signal_names = parsedData.(STR_NAME_TOPIC_DEBUG_FC_SIGNAL_NAMES);
        end
        
        % create new topic and import timestamp vec from debug_fc_signal_values_0
        tvec = parsedData.(STR_NAME_TOPIC_DEBUG_FC_SIGNAL_VALUES).timestamp;
        parsedData(1).(STR_NAME_TOPIC_DEBUG_FC_SIGNALS) = struct([]);
        parsedData(1).(STR_NAME_TOPIC_DEBUG_FC_SIGNALS)(1).timestamp = tvec;
        
        % TODO these constants shouldn't be hardcoded here...
        SIZE_SIGNAL = 15; % number of (float) signals logged
        SIZE_SIGNAL_INT = 3; % number of int signals logged
        SIZE_SIGNAL_BOOL = 3; % number of bool signals logged
        SIZE_SIGNAL_STR = 20; % max length of signal name string
        
        % process (float) variable names
        s = char(topic_debug_signal_names.signal(1,:));
        assert(numel(s) == SIZE_SIGNAL * SIZE_SIGNAL_STR, 'Wrong size of signal name vector.');
        
        for i = 1:SIZE_SIGNAL
            ind = (1:SIZE_SIGNAL_STR) + (i-1)*SIZE_SIGNAL_STR;
            nam = string(s(ind));
            nam = strip(nam, sprintf('\0')); % remove trailing null-characters
            namc = char(nam);
            parsedData(1).(STR_NAME_TOPIC_DEBUG_FC_SIGNALS)(1).(namc) = parsedData.(STR_NAME_TOPIC_DEBUG_FC_SIGNAL_VALUES).values(:,i);
        end
        
        % process int variable names
        s = char(topic_debug_signal_names.signal_int(1,:));
        assert(numel(s) == SIZE_SIGNAL_INT * SIZE_SIGNAL_STR, 'Wrong size of signal name vector.');
        
        for i = 1:SIZE_SIGNAL_INT
            ind = (1:SIZE_SIGNAL_STR) + (i-1)*SIZE_SIGNAL_STR;
            nam = string(s(ind));
            nam = strip(nam, sprintf('\0')); % remove trailing null-characters
            namc = char(nam);
            parsedData(1).(STR_NAME_TOPIC_DEBUG_FC_SIGNALS)(1).(namc) = int8(parsedData.(STR_NAME_TOPIC_DEBUG_FC_SIGNAL_VALUES).values_int(:,i)); % explicit cast to int8
        end
        
        % process bool variable names
        s = char(topic_debug_signal_names.signal_bool(1,:));
        assert(numel(s) == SIZE_SIGNAL_BOOL * SIZE_SIGNAL_STR, 'Wrong size of signal name vector.');
        
        for i = 1:SIZE_SIGNAL_BOOL
            ind = (1:SIZE_SIGNAL_STR) + (i-1)*SIZE_SIGNAL_STR;
            nam = string(s(ind));
            nam = strip(nam, sprintf('\0')); % remove trailing null-characters
            namc = char(nam);
            parsedData(1).(STR_NAME_TOPIC_DEBUG_FC_SIGNALS)(1).(namc) = logical(parsedData.(STR_NAME_TOPIC_DEBUG_FC_SIGNAL_VALUES).values_bool(:,i)); % explicit cast to logical type
        end
        
        if FLAG_REMOVE_RAW_DEBUG % remove the two debug topics from mat file (default true)
            if ~FLAG_EXT_DEBUG_NAMES
                parsedData = rmfield(parsedData, STR_NAME_TOPIC_DEBUG_FC_SIGNAL_NAMES);
            end
            parsedData = rmfield(parsedData, STR_NAME_TOPIC_DEBUG_FC_SIGNAL_VALUES);
        end
        
        N = length(fieldnames(parsedData)); % update number of topics

    end
    
    %%% scale time vector to seconds (contained in the timestamp field of each
    %%% topic)
    if FLAG_SCALE_TIME_VEC
        SCALE_FACTOR = 1e-6; % mu_s to s
        
        topicNames = fieldnames(parsedData);
        
        %%% find initial time instant
        time_in = 1e12;
        
        for i = 1:N
            top = topicNames{i};
            t = parsedData.(top).timestamp;
            if t(1) < time_in
                time_in = t(1);
            end
        end
        
        %%% shift by time_in and scale the timestamp field of each topic
        for i = 1:N
            top = topicNames{i};
            t = parsedData.(top).timestamp;
            
            parsedData.(top).timestamp = (t - time_in) * SCALE_FACTOR;
        end
        
    end
    
    %%% save to .mat file
    if exist(dir_parsed, 'dir') == 0
        mkdir(dir_parsed)
    end
    
    outputFileName = fullfile(dir_parsed, sprintf('%s.mat', out_file));
    
    if exist(outputFileName, 'file') == 2
        delete(outputFileName)
    end
    
    save(outputFileName, '-struct', 'parsedData');
    
    fprintf('\nParsing done. Saving output to\n\n%s\n', outputFileName);
    
    if plot_data
        plotLoggedDataPx4(outputFileName);
    end
end

end

function [topicName, dataStruct] = importCsvData(ulgFileName, csvfileName, str_id_warning)
% extract topic name
% csv file name format:
% [ulgFileName, '_', 'topicName', '_', (value), '.csv']
ii = regexp(csvfileName, ulgFileName, 'once') + length(ulgFileName); % this marks the '_' before the topic name begins
topicName = csvfileName(ii+1:end-4); % remove the '.ulg' 4-char extension at the end of string

warning('off', str_id_warning);
t = readtable(csvfileName, 'ReadVariableNames', true); % this would cast a warning if the variable name in the table contains special characters
varNames = t.Properties.VariableNames;
warning('on', str_id_warning);

dataStruct = struct([]);

cc = 1;
i_scalar = [];
dim_vec = [];
n_vec = 0;
varNames_scalar = {};

while (cc <= length(varNames))
    n = varNames{cc};
    i_vec = regexp(n, '_\d+_'); % if n is an element of a vector , the [i] string is replaced by _i_
    % (the '+' wildcard considers possibly more than one digit)
    
    if ~isempty(i_vec) % vector variable
        str_vec = n(1:i_vec-1); % take the first part of string (i.e. name of vector variable)
        
        if ~strcmp(str_vec, varNames_scalar{end}) % reset counter
            n_vec = 0;
        end
        
        n_vec = n_vec + 1;
        if n_vec == 1
            i_scalar = [i_scalar, cc];
            varNames_scalar = [varNames_scalar, {str_vec}];
            dim_vec = [dim_vec, 1];
        else
            dim_vec(end) = n_vec;
        end
    else % scalar variable
        n_vec = 0;
        i_scalar = [i_scalar, cc];
        varNames_scalar = [varNames_scalar, {n}];
        dim_vec = [dim_vec, 1];
    end
    
    cc = cc+1;
end

for i = 1:length(i_scalar)
    ind = i_scalar(i);
    
    n = varNames_scalar{i};
    len = dim_vec(i);
    
    indRange = (1:len)+ind-1;
    dat = t{:, indRange};
    dataStruct(1).(n) = dat;
end

end

function curDir = getCurrentDirectory()
% GETCURRENTDIRECTORY returns the path in the filesystem of this file

path_routine = mfilename('fullpath'); % get full path name of the current routine
PATH_DEPTH = 0; % the default dirs are located at the same level of this routine
% (e.g., PATH_DEPTH = 1 means a subdirectory wrt the current level)

ind_backslash_vec = regexp(path_routine, filesep);
ind_backslash = ind_backslash_vec(end - PATH_DEPTH);

curDir = path_routine(1:ind_backslash-1);

end

function isLogged = isTopicLogged(loggedTopicNames, STR_NAME_TOPIC)
isLogged = false;

for i = 1:length(loggedTopicNames)
    ss = loggedTopicNames{i};
    if strcmp(STR_NAME_TOPIC, ss)
        isLogged = true;
    end
end

end

%% END OF CODE
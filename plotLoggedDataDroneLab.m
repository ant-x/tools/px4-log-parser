function plotLoggedDataDroneLab( str_fileName )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%% load logged data
r = load(str_fileName);

%% Plot angular rate
figure
subplot(311)
hold on
plot(r.rate.timestamp, r.rate.value(:,1))
plot(r.rate0.timestamp, r.rate0.value(:,1))
hold off
grid on
box on
xlabel('time [s]')
ylabel('p [rad/s]')
legend('measure','setpoint')
title('Angular rate')

subplot(312)
hold on
plot(r.rate.timestamp, r.rate.value(:,2))
plot(r.rate0.timestamp, r.rate0.value(:,2))
hold off
grid on
box on
xlabel('time [s]')
ylabel('q [rad/s]')

subplot(313)
hold on
plot(r.rate.timestamp, r.rate.value(:,3))
plot(r.rate0.timestamp, r.rate0.value(:,3))
hold off
grid on
box on
xlabel('time [s]')
ylabel('r [rad/s]')

%% Plot attitude
figure
subplot(311)
hold on
plot(r.att.timestamp, r.att.value(:,3))
plot(r.att0.timestamp, r.att0.value(:,3))
hold off
grid on
box on
xlabel('time [s]')
ylabel('\phi [rad]')
legend('measure','setpoint')
title('Attitude')

subplot(312)
hold on
plot(r.att.timestamp, r.att.value(:,2))
plot(r.att0.timestamp, r.att0.value(:,2))
hold off
grid on
box on
xlabel('time [s]')
ylabel('\theta [rad]')

subplot(313)
hold on
plot(r.att.timestamp, r.att.value(:,1))
plot(r.att0.timestamp, r.att0.value(:,1))
hold off
grid on
box on
xlabel('time [s]')
ylabel('\psi [rad]')

%% Plot velocity
figure
subplot(311)
hold on
plot(r.vel.timestamp, r.vel.value(:,1))
plot(r.vel0.timestamp, r.vel0.value(:,1))
hold off
grid on
box on
xlabel('time [s]')
ylabel('Vx [m/s]')
legend('measure','setpoint')
title('Velocity')

subplot(312)
hold on
plot(r.vel.timestamp, r.vel.value(:,2))
plot(r.vel0.timestamp, r.vel0.value(:,2))
hold off
grid on
box on
xlabel('time [s]')
ylabel('Vy [m/s]')

subplot(313)
hold on
plot(r.vel.timestamp, r.vel.value(:,3))
plot(r.vel0.timestamp, r.vel0.value(:,3))
hold off
grid on
box on
xlabel('time [s]')
ylabel('Vz [m/s]')

%% Plot position
figure
subplot(311)
hold on
plot(r.pos.timestamp, r.pos.value(:,1))
plot(r.pos0.timestamp, r.pos0.value(:,1))
hold off
grid on
box on
xlabel('time [s]')
ylabel('x [m/s]')
legend('measure','setpoint')
title('Position')

subplot(312)
hold on
plot(r.pos.timestamp, r.pos.value(:,2))
plot(r.pos0.timestamp, r.pos0.value(:,2))
hold off
grid on
box on
xlabel('time [s]')
ylabel('y [m/s]')

subplot(313)
hold on
plot(r.pos.timestamp, r.pos.value(:,3))
plot(r.pos0.timestamp, r.pos0.value(:,3))
hold off
grid on
box on
xlabel('time [s]')
ylabel('z [m/s]')

%% Plot Control actions
figure
subplot(2,1,1)
hold on
plot(r.mom.timestamp, r.mom.value(:,1))
plot(r.mom.timestamp, r.mom.value(:,2))
plot(r.mom.timestamp, r.mom.value(:,3))
hold off
grid on
box on
ylabel('Torque [1]')
legend('L', 'M', 'N')

subplot(2,1,2)
hold on
plot(r.force.timestamp, r.force.value(:,1))
plot(r.force.timestamp, r.force.value(:,2))
plot(r.force.timestamp, r.force.value(:,3))
hold off
grid on
box on
xlabel('time [s]')
ylabel('Thrust [1]')
legend('Fx', 'Fy', 'Fz')

%% Sensors
figure
subplot(2,1,1)
hold on
plot(r.acc.timestamp, r.acc.value(:,1))
plot(r.acc.timestamp, r.acc.value(:,2))
plot(r.acc.timestamp, r.acc.value(:,3))
hold off
title('Control actions')
grid on
box on
ylabel('Acc [m/s]')
title('Sensors')
legend('x', 'y', 'z')

subplot(2,1,2)
hold on
plot(r.gyr.timestamp, r.gyr.value(:,1))
plot(r.gyr.timestamp, r.gyr.value(:,2))
plot(r.gyr.timestamp, r.gyr.value(:,3))
hold off
grid on
box on
ylabel('Gyro [rad/s]')
legend('x', 'y', 'z')

end